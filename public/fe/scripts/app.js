'use strict';
var init_module = [
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'bootstrapLightbox',
    'vcRecaptcha'
];

var app = angular.module('SedonaApp',init_module)
    .run([ '$rootScope','$stateParams', '$state',
        function ($rootScope,   $state,   $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.varialbe = '';
        }
    ])
    .config(
    [          '$stateProvider', '$httpProvider',  '$locationProvider', '$compileProvider',  '$provide', '$interpolateProvider','$urlRouterProvider',
        function ($stateProvider, $httpProvider,   $locationProvider,   $compileProvider,    $provide, $interpolateProvider, $urlRouterProvider) {

            $interpolateProvider.startSymbol('{[{');
            $interpolateProvider.endSymbol('}]}');

            //$urlRouterProvider
            //    .otherwise('/');

            //
            //
            //$stateProvider
            //    .state('success', {
            //        url: "/success",
            //        templateUrl: "/registration/success"
            //    });

        }
    ]
)
