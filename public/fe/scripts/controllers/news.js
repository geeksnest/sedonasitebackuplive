'use strict';

/* Controllers */

app.controller('NewsCtrl', function($scope, $http, appConfig, News, $window, $sce, Lightbox){
    // Disable weekend selection
    var offset = 0;
    var page = 10;
    var list = [];
    $scope.loading = false;
    $scope.newslist = [];
    $scope.hideloadmore = false;
    $scope.showmorenews = function(){
        console.log('showmore');
        $scope.loading = true;

        News.list(offset, page, function(data){

            for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }
            console.log(data);
            list = list.concat(data);
            offset = offset + page;
            $scope.loading = false;
            //console.log(text.match((src="https://www.youtube.com/embed/(.*?)")e));
            console.log(list);
            $scope.newslist = list;
            if(data.length < 10){
                $scope.hideloadmore = true;
            }
        });
    }

    $scope.showmorenewsbycat = function(){
        console.log('showmore');
        $scope.loading = true;
        var url = window.location.href;
        var cat = url.match(/\/blog\/category\/(.*)+/);

        News.listbycat(cat[1],offset, page, function(data){
            for(var key in data){
                var categorylist = [];
                data[key].categorylist.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }
            list = list.concat(data);
            offset = offset + page;
            $scope.loading = false;
            //console.log(text.match((src="https://www.youtube.com/embed/(.*?)")e));
            console.log(list);
            $scope.newslist = list;
            $scope.categoryName = data[0].categoryname;
            console.log($scope.categoryName);
            if(data.length < 10){
                $scope.hideloadmore = true;
            }
        });
    }

    $scope.redirectNews = function(link){
        $window.location.href = '/blog/' + link;
    }

    $scope.getauthor = function(){
        console.log('showmore');
        $scope.loading = true;
        var url = window.location.href;
        var auth = url.match(/\/blog\/author\/(.*)+/);

        News.author(auth[1],offset, page, function(data){
            for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }
            list = list.concat(data);
            offset = offset + page;
            $scope.loading = false;
            //console.log(text.match((src="https://www.youtube.com/embed/(.*?)")e));
            console.log(list);
            $scope.newslist = list;


            $scope.authorImage = data[0].image;
            $scope.location = data[0].location;
            $scope.occupation = data[0].occupation;
            $scope.name = data[0].name;
            $scope.datecreated = data[0].datecreated;
            $scope.about = $sce.trustAsHtml(data[0].about);

            if(data.length < 10){
                $scope.hideloadmore = true;
            }
        });
    }

    $scope.gettags = function(){
        console.log('==================================================tags');
        $scope.loading = true;
        var url = window.location.href;
        var tag = url.match(/\/blog\/tags\/(.*)+/);

        News.listbytag(tag[1],offset, page, function(data){
            for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }
            list = list.concat(data);
            offset = offset + page;
            $scope.loading = false;
            //console.log(text.match((src="https://www.youtube.com/embed/(.*?)")e));
            $scope.newslist = list;

            $scope.tagname = data[0].tags;

            if(data.length < 10){
                $scope.hideloadmore = true;
            }
        });
    }

    $scope.getarchives = function() {
        console.log("archives");
        $scope.loading = true;
        var url = window.location.href;
        var archive = url.match(/\/blog\/archive\/(.*)+\/(.*)+/);

        News.listbyarchive(archive[1], archive[2], offset, page, function(data){
            for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }
            list = list.concat(data);
            offset = offset + page;
            $scope.loading = false;

            $scope.newslist = list;

            $scope.archive = archive[1] + " " + archive[2];
            if(data.length < 10){
                $scope.hideloadmore = true;
            }
        });
    }

    console.log('OPEN OPEN');

    $scope.openlightbox = function(image){
        console.log(image);
        var img = [{'url': "https://sedonahealing.s3.amazonaws.com/uploads/newsimage/me.png"}];
        Lightbox.openModal(img, 0);
    }

})