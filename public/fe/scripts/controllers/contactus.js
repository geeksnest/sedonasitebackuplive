'use strict';

/*controller*/
app.controller('ContactusCtrl', function($scope, $state, $http, appConfig, vcRecaptchaService, $timeout) {

  $scope.validemail = false;
  $scope.res = false;
  $scope.onemail = function(email) {

    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(email);
    if (validemail == false) {
      $scope.validemail = false;
      $('.booking-email').removeClass('hidden');
      $('#email').attr('class', 'form-control textbox ng-dirty ng-valid-parse ng-touched ng-invalid ng-invalid-required');
    } else {
      $scope.validemail = true;
      $('.booking-email').addClass('hidden');
      $('#email').attr('class', 'form-control textbox ng-dirty ng-valid-parse ng-valid ng-valid-required ng-touched');
    }
  }

  $scope.submit = function(feedback) {
    $scope.imageloader = true;
    $http({
      url: appConfig.ResourceUrl + "/contactus/save",
      method: "post",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(feedback)
    }).success(function(data, status, headers, config) {
       $scope.imageloader = false;
      angular.element('.feedback-success').removeClass('hidden');
      window.scrollTo(0, 100);
      $scope.feedback = {};
      console.log($scope.feedback);
      $scope.formContactUs.$setPristine();
      $scope.formContactUs.$invalid = true;
      vcRecaptchaService.reload($scope.widgetId);
      $scope.res = false;
      $timeout(function(){
          angular.element('.feedback-success').addClass('hidden');
      }, 10000);
    }).error(function(data, status, headers, config) {
      console.log(data);
    });
  };

  $scope.setResponse = function(response) {
    console.info('Response available');
    $scope.response = response;
    $scope.res = true;
  };
  $scope.setWidgetId = function(widgetId) {
    console.info('Created widget ID: %s', widgetId);
    $scope.widgetId = widgetId;
  };
  $scope.cbExpiration = function() {
    console.info('Captcha expired. Resetting response object');
    $scope.response = null;
  };
})
