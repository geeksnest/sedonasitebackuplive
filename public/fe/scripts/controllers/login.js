'use strict';

/* Controllers */

app.controller('LoginCtrl', function($scope, $state, $anchorScroll, $location, User, store, $window, Login){

    console.log('==== Login Page ====');

    Login.redirectToMainifLogin();

    $scope.formStatus = false;
    $scope.master ={};

    $scope.phoneNumberPattern = (function() {
        var regexp = /^[a-zA-Z0-9]+$/;
        return {
            test: function(value) {
                if( $scope.requireTel === false ) {
                    return true;
                }
                return regexp.test(value);
            }
        };
    })();

    $scope.reset = function(form){
        $scope.$broadcast('show-errors-reset');
    }
    $scope.loginerror = '';
    $scope.login = function(log){
            console.log('SAVE IT');
            User.login(log, function(data){
                console.log(data);
                if(!data.hasOwnProperty('error')){
                    $scope.formStatus = true;
                    store.set('jwt', data.data);
                    $window.location = '/';

                }else{
                    $scope.loginerror = data.error;
                    console.log(data.error);
                }
            });
    }

})