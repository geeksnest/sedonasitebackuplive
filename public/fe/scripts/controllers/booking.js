'use strict';

/* Controllers */

app.controller('BookingCtrl', function($scope, $state, $http, appConfig){
    // Disable weekend selection
$scope.book = {
    };

    var oribook = angular.copy($scope.book);

    console.log('==== Booking Page ====');
    $scope.successmsg = false;

    $scope.submit = function(book) {
        // console.log("modal");

        $scope.imageloader = true;
        $http({
            url: appConfig.ResourceUrl + "/booking/send",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(book)
        }).success(function (data, status, headers, config) {
            console.log("send");
            window.scrollTo(0, 100);
            $scope.successmsg = true;
            $scope.imageloader = false;
            $('.booking-success').removeClass('hidden');
            $scope.formBooking.$setPristine();
            $scope.book = angular.copy(oribook);
            
        }).error(function (data, status, headers, config) {
            console.log("error");
        });

    }

    $scope.validemail = false;
    $scope.onemail = function(email) {

        var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(email);
        if(validemail == false){
            $scope.validemail = false;
            $('.booking-email').removeClass('hidden');
            $('#email').attr('class','form-control textbox ng-dirty ng-valid-parse ng-touched ng-invalid ng-invalid-required');
        }
        else{
            $scope.validemail = true;
            $('.booking-email').addClass('hidden');
            $('#email').attr('class','form-control textbox ng-dirty ng-valid-parse ng-valid ng-valid-required ng-touched');
        }
    }

    $scope.validphonenumber = false;
    $scope.onphonenum = function(phonenum){
        var validphonenumber = /^[0-9\s\-()+\.]+$/.test(phonenum);
        if(validphonenumber == false){
            $scope.validphonenumber = false;
            $('.booking-phonenum').removeClass('hidden');
            $('#phonenumber').attr('class','form-control textbox ng-dirty ng-valid-parse ng-touched ng-invalid ng-invalid-required');
            /*$('#phonenumber').closest('div').find('.validation-icon').addClass('fa-times').removeClass('fa-check');*/
        }else {
            $scope.validphonenumber = true;
            $('.booking-phonenum').addClass('hidden');
            $('#phonenumber').attr('class','form-control textbox ng-dirty ng-valid-parse ng-valid ng-valid-required ng-touched');
            /*$('#phonenumber').closest('span').find('.validation-icon').addClass('fa-check').removeClass('fa-times');*/
        }
    }

})
