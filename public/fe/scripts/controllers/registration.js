'use strict';

/* Controllers */

app.controller('RegistrationCtrl', function($scope, BirthDayMonthYear, vcRecaptchaService, User , $state, $anchorScroll, $location, Login){
    // Disable weekend selection
    console.log('==== Registration Page ====');
    Login.redirectToMainifLogin();
    $scope.response = null;
    $scope.widgetId = null;
    $scope.model = {
        key: '6Lc6HgQTAAAAAJ5MSwQVXBAmhV1sWUeT-4LZrFnT'
    };
    $scope.setResponse = function (response) {
        console.info('Response available');
        $scope.response = response;
    };
    $scope.setWidgetId = function (widgetId) {
        console.info('Created widget ID: %s', widgetId);
        $scope.widgetId = widgetId;
    };

    $scope.formStatus = false;
    $scope.master ={};
    $scope.months = BirthDayMonthYear.month();
    $scope.day = BirthDayMonthYear.day();
    $scope.year = BirthDayMonthYear.year();
    $scope.phoneNumberPattern = (function() {
        var regexp = /^[a-zA-Z0-9]+$/;
        return {
            test: function(value) {
                if( $scope.requireTel === false ) {
                    return true;
                }
                return regexp.test(value);
            }
        };
    })();
    $scope.reg = { 'gendererror' : false,
                    'termserror' : false,
                    'newsletter' : 1 }

    $scope.reset = function(form){
        $scope.$broadcast('show-errors-reset');
        $('.strength_meter').find('div').attr("class","");
        $('.strength_meter').find('div').text('Stregth');
    }
    $scope.captcharesponse = false;
    $scope.register = function(reg){
        console.log(reg);
        var gen = !reg.hasOwnProperty('gender') ? $scope.reg['gendererror'] = true : $scope.reg['gendererror'] = false;
        var ter = !reg.hasOwnProperty('terms') ? $scope.reg['termserror'] = true : $scope.reg['termserror'] = false ;
            //console.log(gen);
            //console.log(ter);
        $scope.captcharesponse = vcRecaptchaService.getResponse() == '' ? true : false;
        console.log($scope.captcharesponse);
        if(!gen && !ter && !$scope.captcharesponse){
            $scope.formStatus = true;
            console.log('SAVE IT');
            User.register(reg, function(data){
                if(data.hasOwnProperty('success')){
                    console.log('should change to success');
                    $location.hash('registrationpage');
                    $state.go('success');

                }
            });
        }
    }

})