app.factory('Maps', function($http, store, jwtHelper, $upload, $q, rfc4122){
    return {
        currentPin: null,
        currentMap: null,
        markerPanel: false,
        markers: [],
        pinImages: [],
        showDelete: false,
        setCurrentPin: function(val){
            this.currentPin = val;
        },
        setCurrentMap: function(val){
            this.currentMap = val;
        },
        getMarkerKey: function(obj, val){
            for(var i in obj){
                if(obj[i].hasOwnProperty['id']){
                    if(obj[i][prop] == val){
                        return i;
                    }
                }
                if(obj[i].hasOwnProperty['idKey']){
                    if(obj[i][prop] == val){
                        return i;
                    }
                }

            }
        },
        getToggleMarkerInfo: function(){
            return this.markerPanel;
        },
        setToggleMarkerInfo: function(bol){
            return this.markerPanel = bol;
        },
        saveMarkerPics: function(files, idKey){
            var promises;
            files.map(function(file) {
                promises = $upload.upload({
                    url: 'https://planetimpossible.s3.amazonaws.com/', //S3 upload url including bucket name
                    method: 'POST',
                    transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                    },
                    fields : {
                      key: 'uploads/markers/'+ idKey + '/'+file.name, // the key to store the file on S3, could be file name or customized
                      AWSAccessKeyId: 'AKIAILMHXAWGTTIYVASQ',
                      acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                      policy: 'ewogICJleHBpcmF0aW9uIjogIjIwMjAtMDEtMDFUMDA6MDA6MDBaIiwKICAiY29uZGl0aW9ucyI6IFsKICAgIHsiYnVja2V0IjogInBsYW5ldGltcG9zc2libGUifSwKICAgIFsic3RhcnRzLXdpdGgiLCAiJGtleSIsICJ1cGxvYWRzLyJdLAogICAgeyJhY2wiOiAicHJpdmF0ZSJ9LAogICAgWyJzdGFydHMtd2l0aCIsICIkQ29udGVudC1UeXBlIiwgIiJdLAogICAgWyJjb250ZW50LWxlbmd0aC1yYW5nZSIsIDAsIDEwNDg1NzZdCiAgXQp9', // base64-encoded json policy (see article below)
                      signature: 'gpOfdTwUE4H1AWaYMvh2nsS6G58=', // base64-encoded signature based on policy string (see article below)
                      "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                    },
                    file: file
                  });

                promises.then(function(){
                    console.log('THEN PROMISES');
                });
            });
            return $q.all(promises);
        },
        showTrueMarker: function(id){
            for(var x in this.markers){
                if(this.markers[x].idKey == id){
                    this.markers[ x ].show=true;
                    return x;
                }
            }
        },
        convertImageSize: function(bytes){
           if(bytes == 0) return 0;
           var k = 1000;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           return (bytes / Math.pow(k, i)).toPrecision(3);
        },
        pin:  {
            url: '/img/pin.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(23, 34),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0,0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(4, 34)
          },
        kingpin: {
            url: '/img/kingpin.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(23, 43),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0,0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(4, 43)
          }

    }
})