'use strict';

/* Controllers */

app.filter('returnYoutubeThumb', function(){
    return function (item) {
        if(item){
        var newdata = item;
        var x;
            x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
            return 'http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
        }else{
            return x;
        }
    };
});

app.filter('returnImageThumb', function(appConfig){
    return function (item) {
        if(item){
            return appConfig.ImageLinK + "/uploads/newsimage/" + item;
        }else{
            return item;
        }
    };
})