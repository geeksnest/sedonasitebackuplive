app.factory('Contactus', function($http, $q, Config, $filter){
    return {
        paging: function (num, off, keyword,searchdate ,callback) {
            $http({
                url: Config.ApiURL + "/contactus/list/" + num + '/' + off + '/' + keyword + '/' + searchdate,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        delete: function(id, callback){
            $http({
                url: Config.ApiURL + "/contactus/delete/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        get: function(id, callback){
            $http({
                url: Config.ApiURL + "/contactus/get/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        read: function(id, callback){
            $http({
                url: Config.ApiURL + "/contactus/read/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        },
        getreplies: function(id, callback) {
            $http({
                url: Config.ApiURL + "/contactus/getreplies/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        reply: function(data, id, callback){
            var newdata = {
                'message': data,
                'contactusid': id
            };
            $http({
                url: Config.ApiURL + "/contactus/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(newdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
    }

})
