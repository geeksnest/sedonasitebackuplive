app.factory('Settings', function($http, $q, Config, $filter) {
	return {
        get : function(callback){
            $http({
                url: Config.ApiURL + "/settings/load",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
		updatescript: function(data, callback) {
			$http({
                url: Config.ApiURL + "/settings/update/googlescript",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                $modalInstance.close();
            });
		},
        savemainte : function(mainte, callback){
            $http({
                url: Config.ApiURL + "/settings/save/maintenance",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data : $.param(mainte)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        offmainte : function(callback){
            $http({
                url: Config.ApiURL + "/settings/off/maintenance",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        }
	}
})