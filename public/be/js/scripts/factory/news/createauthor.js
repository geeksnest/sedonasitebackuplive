app.factory('Createauthor', function($http, $q, Config){
  	return {

  		data: {},
    	loadcategory: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listcategory",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	}

    }
   
})