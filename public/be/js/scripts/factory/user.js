app.factory('User', function($http, $q, Config, Upload){
    return {

        usernameExists: function(username, id, callback) {
            $http({
                url: Config.ApiURL +"/validate/username/"+username+"/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        chkpass: function(password, id, callback){
            $http({
                url: Config.ApiURL +"/validate/password/"+id+"/"+password,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        emailExists: function(email, id, callback) {
            $http({
                url: Config.ApiURL +"/validate/email/"+email+"/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                // data: $.param(dlt)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        register:function(user, callback){
            $http({
                url: Config.ApiURL + "/user/register",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(user)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        list: function(num, off, keyword, id, callback){
            $http({
                url: Config.ApiURL +"/user/list/" + num + '/' + off + '/' + keyword + '/' + id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        delete:function(user, callback){
            $http({
                url:Config.ApiURL +"/user/delete/"+user,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            })
        },
        info : function(userid, callback) {
            $http({
                url: Config.ApiURL +"/user/info/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
               callback(data);
            }).error(function (data, status, headers, config) {
               callback(data);
            })
        },
        uploadpic : function(files, callback){
            var file = files[0];
            var promises;
            promises = Upload.upload({
                url: Config.amazonlink, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                },
                fields: {
                    key: 'uploads/userimages/' + file.name, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: Config.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: Config.policy, // base64-encoded json policy (see article below)
                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                var log = progressPercentage;
                callback(log);
            })
        },
        update:function(user, callback){
            $http({
                url: Config.ApiURL +"/user/update",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config){
                callback(data);
            });
        },
        login: function(log, callback){
            $http({
                url: 'http://pi.api/user/login',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(log)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        },
        updatestatus: function(id, newstat, callback){
            $http({
                url: Config.ApiURL +"/user/updatestatus/" + id + "/" + newstat,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config){
                callback(data);
            });
        }
    }

})
