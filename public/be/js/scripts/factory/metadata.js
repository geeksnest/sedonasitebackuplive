app.factory('Meta', function($http, $q, Config){
    return {

         
         data: {},
         sample: function(num, off, keyword , callback){
            $http({
                        url: Config.ApiURL +"/meta/managepage/" + num + '/' + off + '/' + keyword,
                        method: "GET",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                       data = data;
                       callback(data);
                       pagetotalitem = data.total_items;
                       currentPage = data.index;
                       
                    }).error(function (data, status, headers, config) {
                        
                    });
         },
         edit: function(meta, callback){
            $http({
                url: Config.ApiURL +"/meta/edit",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(meta)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {

            });
        },


    }
   
})