app.factory('Managepages', function($http, $q, Config){
  	return {
    	 data: {},
    	 sample: function(num, off, keyword , callback){
    	 	$http({
			            url: Config.ApiURL +"/pages/managepage/" + num + '/' + off + '/' + keyword,
			            method: "GET",
			            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			        }).success(function (data, status, headers, config) {
			           data = data;
			           callback(data);
			           pagetotalitem = data.total_items;
			           currentPage = data.index;
			           
			        }).error(function (data, status, headers, config) {
			           callback(data);
			        });
    	 },
		get: function(callback){
			$http({
				url: Config.ApiURL +"/pages/get",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				console.log(data);
				callback(data);
			}).error(function (data, status, headers, config) {

			});
		},
		updatestatus: function(pageid, keyword, status, callback){
			$http({
				url: Config.ApiURL + "/pages/updatepagestatus/"+status+"/" + pageid + '/' + keyword,
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		delete: function(pageid, callback){
			$http({
				url: Config.ApiURL + "/page/pagedelete/" + pageid,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		}
    }
   
})