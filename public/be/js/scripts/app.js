'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.factory',
    'app.directives',
    'app.controllers',
    'ngFileUpload',
    'xeditable',
    'angular.chosen',
    'angularMoment',
    'ui.select',
    'ngSanitize',
    'colorpicker.module'
  ])

.run(['$rootScope', '$state', '$stateParams', 'editableOptions', function ($rootScope,   $state,   $stateParams,editableOptions) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    editableOptions.theme = 'bs3';
}])

.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider' ,
  function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider ) {

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.factory    = $provide.factory;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');


        $urlRouterProvider
        .otherwise('/dashboard');

        $stateProvider

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '/sedonaadmin/admin/dashboard',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/dashboard.js'
                        ]);
                }]
            }
        })
        /*USER STATE*/
        .state('userscreate', {
            url: '/userscreate',
            templateUrl: '/sedonaadmin/users/create',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users/createuser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('userlist', {
            url: '/userlist/:userid',
            controller: 'UserListCtrl',
            templateUrl: '/sedonaadmin/users/userlist',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users/manageusers.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('updateuser', {
            url: '/updateuser/:userid',
            templateUrl: '/sedonaadmin/users/edituser',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users/edituser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('logout', {
            url: '/updateuser/:userid',
             templateUrl: '/sedonaadmin/admin/logout',

        })
        /*END USER STATE*/


            //rainier state


            //start createpage
            .state('createpage', {
                url: '/createpage',
                controller: 'Createpage',
                templateUrl: '/sedonaadmin/pages/createpage',

            })
            //end createpage

            //start managepage
            .state('managepage', {
                url: '/managepage',
                controller: 'Managepage',
                templateUrl: '/sedonaadmin/pages/managepage',

            })
            //end managepage

            //start managepagecategory
            .state('managepagecategory', {
                url: '/managepagecategory',
                controller: 'Managepagecategory',
                templateUrl: '/sedonaadmin/pages/managepagecategory',

            })
            //end managepagecategory

            //start editpage
            .state('editpage', {
                url: '/editpage/:pageid',
                controller: 'Editpage',
                templateUrl: '/sedonaadmin/pages/editpage',

            })
            //end editpage

            //start createnews
            .state('createnews', {
                url: '/createnews',
                controller: 'Createnews',
                templateUrl: '/sedonaadmin/news/createnews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/createnews.js',
                      '/be/js/scripts/factory/news/createnews.js',
                        '/be/js/scripts/factory/news/tags.js',
                        '/be/js/scripts/factory/news/category.js',
                        '/be/js/scripts/filter.js'
                      ]);
                  }]
                }

            })
            //end createnews

            //start managenews
            .state('managenews', {
                url: '/managenews',
                controller: 'Managenews',
                templateUrl: '/sedonaadmin/news/managenews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/managenews.js',
                      '/be/js/scripts/factory/news/managenews.js'
                      ]);
                  }]
                }

            })
            //end managenews

            //start editnews
            .state('editnews', {
                url: '/editnews/:newsid',
                controller: 'Editnews',
                templateUrl: '/sedonaadmin/news/editnews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/editnews.js',
                      '/be/js/scripts/factory/news/createnews.js',
                        '/be/js/scripts/factory/news/tags.js',
                        '/be/js/scripts/factory/news/category.js'
                      ]);
                  }]
                }

            })
            //end editnews

            //start tags
            .state('tags', {
                url: '/tags',
                controller: 'Tags',
                templateUrl: '/sedonaadmin/news/tags',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/tags.js',
                        '/be/js/scripts/factory/news/tags.js'
                      ]);
                  }]
                }
            })
            //end tags

            //start tags
            .state('category', {
                url: '/category',
                controller: 'Category',
                templateUrl: '/sedonaadmin/news/category',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/category.js',
                        '/be/js/scripts/factory/news/category.js'
                      ]);
                  }]
                }
            })
            //end tags

            //start Create Author
            .state('createauthor', {
                url: '/createauthor',
                controller: 'Createauthor',
                templateUrl: '/sedonaadmin/news/createauthor',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/createauthor.js',
                      '/be/js/scripts/factory/news/createauthor.js'
                      ]);
                  }]
                }
            })
            //end Create Author

            //start Manage Author
            .state('manageauthor', {
                url: '/manageauthor',
                controller: 'Manageauthor',
                templateUrl: '/sedonaadmin/news/manageauthor',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/manageauthor.js',
                      '/be/js/scripts/factory/news/manageauthor.js'
                      ]);
                  }]
                }
            })

            //start Edit Author
            .state('editauthor', {
                url: '/editauthor:authorid',
                controller: 'Editauthor',
                templateUrl: '/sedonaadmin/news/editauthor',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/editauthor.js',
                      '/be/js/scripts/factory/news/createauthor.js'
                      ]);
                  }]
                }
            })
            //end Edit Author

            //start testi
            .state('testimonials', {
                url: '/testimonials',
                controller: 'TestimonialsCTRL',
                templateUrl: '/sedonaadmin/testimonials',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/testimonials.js',
                                '/be/js/scripts/factory/testimonials.js'
                            ]);
                        }]
                }
            })
            //end testi

            //start booking
            .state('booking', {
                url: '/booking',
                controller: 'BookingCTRL',
                templateUrl: '/sedonaadmin/booking',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/booking.js',
                                '/be/js/scripts/factory/booking.js'
                            ]);
                        }]
                }
            })
            //end booking

            //start metadata
            .state('metadata', {
                url: '/metadata',
                controller: 'MetaCTRL',
                templateUrl: '/sedonaadmin/metadata/index',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/metadata.js',
                                '/be/js/scripts/factory/metadata.js'
                            ]);
                        }]
                }
            })
            //end metadata

            //start settings
            .state('settings', {
                url: '/settings',
                controller: 'SettingsCTRL',
                templateUrl: '/sedonaadmin/settings',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/settings.js',
                                '/be/js/scripts/factory/settings.js'
                            ]);
                        }]
                }
            })
            //end settings

            //start settings
            .state('contactus', {
                url: '/contactus',
                controller: 'ContactusCtrl',
                templateUrl: '/sedonaadmin/contactus',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/contactus.js',
                                '/be/js/scripts/factory/contactus.js'
                            ]);
                        }]
                }
            })
            //end settings

            //Profile
            .state('profile', {
                url: '/profile/:userid',
                controller: 'ProfileCtrl',
                templateUrl: '/sedonaadmin/profile',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                          '/be/js/scripts/controllers/profile.js',
                          '/be/js/scripts/factory/user.js'
                        ]);
                    }]
                }
            })
    }
    ]
    )

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/be/js/jsons/',
    suffix: '.json'
});

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
 .constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.resize.js',
    '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.spline.js',
    '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
    '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/jquery.nestable.js',
    '/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
    '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
    '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
    '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
    '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
    '/be/js/jquery/footable/footable.core.css']
}
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
    '/be/js/jquery/select2/select2-bootstrap.css',
    '/be/js/jquery/select2/select2.min.js',
    '/be/js/modules/ui-select2.js']
}
)
;
