'use strict';

app.controller('SettingsCTRL', function($scope, $state ,$q, $http, Config, $timeout, $modal, $filter, Settings) {
	

	var getsettings = function(){
		Settings.get(function(data){
			$scope.google = {"script" :data.gscript.script};

			if(data.maintenance.status == 1){	
				$scope.status = true;
				$scope.mainte = { status : true, message : data.maintenance.message, hour : data.maintenance.hour };
				$scope.required = false;
			}else {
				$scope.status = false;
				$scope.mainte = { status : false };
				$scope.required = true;
			}
			console.log($scope.mainte);
		});
	}


	var loadscript = function() {
		Settings.loadscript(function(data){
			$scope.google = {"script" :data[0].script};
		});
	}

	getsettings();

	/*maintenance*/
	$scope.savemainte = function(mainte){
		var msg = mainte.message;
		Settings.savemainte(mainte, function(data){
			if(data == "Success"){
				$scope.status = true;
				$scope.mainte = { status : true, message : msg };
				$scope.required = false;
			}
		});
		$scope.maintenance.$setPristine();
	}

	$scope.offmainte = function(){
		Settings.offmainte(function(data){
			if(data == "Maintenance successfully turned off."){
				$scope.status = false;
				$scope.mainte = { status : false };
				$scope.required = true;
			}
		});
	}

	/*googlescript*/
	$scope.alerts = [];

	$scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

	$scope.savescript = function(google) {
		console.log(google);
		Settings.updatescript(google, function(){
			$scope.alerts = [{ type: 'success', msg: 'Script saved.' }];
		});
	}	
})