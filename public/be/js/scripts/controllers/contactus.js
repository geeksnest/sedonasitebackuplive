'use strict';

/* Controllers */

app.controller('ContactusCtrl', function($scope, $state, $q, $http, Config, $timeout, $modal, $filter, Contactus) {
  $scope.data = {};
  $scope.searchdate = null;
  $scope.keyword = null;
  $scope.loading = false;
  var off = 1;
  var num = 10;
  var keyword = null;
  var searchdate = null;

  var paginate = function(num, off, keyword, searchdate) {
    $scope.loading = true;
    if (keyword === '') {
      keyword = null;
    }
    if (searchdate === '') {
      searchdate = null;
    }
    Contactus.paging(num, off, keyword, searchdate, function(data) {
      $scope.data = data;
      $scope.loading = false;
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      console.log(data);
    });
  }

  paginate(num, off, keyword, searchdate);

  $scope.setPage = function(pageNo) {
    paginate(10, pageNo, $scope.keyword, $scope.searchdate);
  };

  $scope.search = function(keyword, searchdate) {
    var off = 1;
    console.log('Searching...');
    if (searchdate) {
      searchdate = $filter('date')(searchdate, 'yyyy-MM-dd');
      console.log(searchdate);
    } else {
      searchdate = null;
    }
    $scope.keyword = keyword;
    $scope.searchdate = searchdate;

    paginate(num, off, $scope.keyword, searchdate);

    $scope.searchtext = '';
    $scope.searchdateelem = '';
    $scope.formcontactus.$setPristine();
  }

  $scope.clear = function() {
    $scope.dt = null;
  };

  /* Date picker */
  $scope.today = function() {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.clear = function() {
    $scope.keyword = null;
    $scope.searchdate = null;
    paginate(10, 1, $scope.keyword, $scope.searchdate);
  }

  /* DELETE Message */
  var deleteMsgCtrl = function($scope, $modalInstance, id, $state) {
    $scope.msg = 'Are you sure you want to delete this message?';
    $scope.showbuttons = true;
    $scope.ok = function() {
      Contactus.delete(id, function(result) {
        console.log(result);
        if (result.hasOwnProperty('success')) {
          $scope.msg = 'Message has been deleted.';
          $timeout(function() {
            $scope.cancel();
          }, 2000);
        } else {
          $scope.msg = 'Error occured.';
          $timeout(function() {
            $scope.cancel();
          }, 2000);
        }
        $scope.showbuttons = false;
      });
    }
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.deleteMsg = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'contactusDelete.html',
      controller: deleteMsgCtrl,
      resolve: {
        id: function() {
          return id;
        }
      }
    }).result.finally(function() {
      $state.go($state.current, {}, {
        reload: true
      });
    });
  }

  /* REPLY Message */
  var MsgReplyCtrl = function($scope, $modalInstance, id, $state) {
    $scope.contact = '';
    $scope.contactusrep = '';
    $scope.message = '';
    $scope.saving = false;
    Contactus.get(id, function(data) {
      $scope.contact = data[0];
      console.log($scope.contact);
      if ($scope.contact.status == 'new') {
        Contactus.read(id);
      }
      Contactus.getreplies(id, function(data) {
        $scope.contactusrep = data;
        console.log(data);
      });
    });
    $scope.objectKeys = function(data) {
      return Object.keys(data);
    }
    $scope.sendreply = function(data) {
      $scope.saving = true;
      Contactus.reply(data, id, function(data) {
        console.log("Clearing MEssage");
        $scope.message = '';
        console.log(id);
        Contactus.getreplies(id, function(data) {
          $scope.saving = false;
          $scope.contactusrep = data;

        });
      });
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.replyMsg = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'contactusReply.html',
      controller: MsgReplyCtrl,
      resolve: {
        id: function() {
          return id;
        }
      }
    }).result.finally(function() {
      $state.go($state.current, {}, {
        reload: true
      });
    });
  }

})
