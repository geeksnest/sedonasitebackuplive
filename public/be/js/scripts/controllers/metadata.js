'use strict';

/* Controllers */

app.controller('MetaCTRL', function($scope, $state ,$q, $http, Config, $log, Meta, $interval, $modal){
    $scope.keyword=null;
    $scope.loading = false;
    $scope.alerts = [];

    $scope.inline = true;
    $scope.editinline = function () {
        console.log("inline");
        $scope.inline = false;
    }

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    var paginate = function(num,off, keyword){
        Meta.sample(num,off, keyword, function(data){
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    paginate(num,off,keyword);


    $scope.search = function (keyword) {
        $scope.keyword = keyword;
        paginate(10,1,$scope.keyword);
        $scope.searchtext = '';
    }

    $scope.setPage = function (pageNo) {
        paginate(10,pageNo,$scope.keyword);
    };

    $scope.clear = function (pageNo) {
        $scope.keyword= null;
        paginate(10,1,$scope.keyword);
    };


    var successloadalert = function(){
            $scope.alerts = [{ type: 'success', msg: 'Category successfully Deleted!' }];
    }

    var errorloadalert = function(){
            $scope.alerts = [{ type: 'danger', msg: 'Something went wrong Page not Deleted!' }];
    }


    $scope.deletecategory = function(catid) {

        var modalInstance = $modal.open({
            templateUrl: 'pagecategoryDelete.html',
            controller: function($scope, $modalInstance, catid) {
                var datalength = 0;

                var loadconflict = function(){
                    Managepagescategory.loadcatpages(catid, function(data){
                        $scope.pageconflict = data.dataconflict;
                        $scope.categories = data.catpages;
                        datalength = data.dataconflict.length;
                        console.log(data.catpages);
                        if(datalength > 0){
                            $scope.viewconflicts = true;
                            $scope.disabledelete = true;
                        }else {
                            $scope.viewconflicts = false;
                            $scope.disabledelete = false;
                        }
                    });
                }

                loadconflict(); 

                $scope.editcategory = function(page){
                    if(page.catid != undefined){
                        Managepagescategory.updateconflict(page, function(data){
                            console.log(data);
                            loadconflict();
                        });
                    }else {
                        console.log("undefined");
                    }
                }

                $scope.ok = function() {
                    Managepagescategory.delete(catid, function(data){
                        paginate(10,1,$scope.keyword);
                        $modalInstance.close();
                        successloadalert();
                    });

                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                catid: function() {
                    return catid
                }
            }
        });
    }


    $scope.edit = function(meta) {
        var modalInstance = $modal.open({
            templateUrl: 'updateMeta.html',
            controller: function($scope, $modalInstance, $state, meta) {
                $scope.book={
                    title:meta.title,
                    metatitle:meta.metatitle,
                    metakeyword:meta.metakeyword,
                    metadesc:meta.metadesc,
                    id:meta.id,
                    module:meta.module
                }
                $scope.closeAlert = function (index) {
                    $scope.alerts.splice(index, 1);
                };
                $scope.ok = function(meta) {
                    Meta.edit(meta,function(data){
                        paginate(10,1,$scope.keyword);
                        $scope.alerts = [{ type: 'success', msg: 'Meta data successfully Edited!' }];
                    });

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                meta: function() {
                    return meta
                }
            }
        });
    }

    

})