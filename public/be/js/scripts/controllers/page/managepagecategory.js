'use strict';

/* Controllers */

app.controller('Managepagecategory', function($scope, $state ,$q, $http, Config, $log, Managepagescategory, $interval, $modal){
    $scope.keyword=null;
    $scope.loading = false;
    $scope.alerts = [];

    $scope.inline = true;
    $scope.editinline = function () {
        console.log("inline");
        $scope.inline = false;
    }


    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    var paginate = function(num,off, keyword){
        Managepagescategory.sample(num,off, keyword, function(data){
            $scope.data = data;

            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    paginate(num,off,keyword);


    $scope.search = function (keyword) {
        $scope.keyword = keyword;
        paginate(10,1,$scope.keyword);
        $scope.searchtext = '';
    }

    $scope.setPage = function (pageNo) {
        paginate(10,pageNo,$scope.keyword);
    };

    $scope.clear = function (pageNo) {
        $scope.keyword= null;
        paginate(10,1,$scope.keyword);
    };


    var successloadalert = function(){
            $scope.alerts = [{ type: 'success', msg: 'Category successfully Deleted!' }];
    }

    var errorloadalert = function(){
            $scope.alerts = [{ type: 'danger', msg: 'Something went wrong Page not Deleted!' }];
    }


    $scope.deletecategory = function(catid) {

        var modalInstance = $modal.open({
            templateUrl: 'pagecategoryDelete.html',
            controller: function($scope, $modalInstance, catid) {
                var datalength = 0;

                var loadconflict = function(){
                    Managepagescategory.loadcatpages(catid, function(data){
                        $scope.pageconflict = data.dataconflict;
                        $scope.categories = data.catpages;
                        datalength = data.dataconflict.length;
                        console.log(data.catpages);
                        if(datalength > 0){
                            $scope.viewconflicts = true;
                            $scope.disabledelete = true;
                        }else {
                            $scope.viewconflicts = false;
                            $scope.disabledelete = false;
                        }
                    });
                }

                loadconflict(); 

                $scope.editcategory = function(page){
                    if(page.catid != undefined){
                        Managepagescategory.updateconflict(page, function(data){
                            console.log(data);
                            loadconflict();
                        });
                    }else {
                        console.log("undefined");
                    }
                }

                $scope.ok = function() {
                    Managepagescategory.delete(catid, function(data){
                        paginate(10,1,$scope.keyword);
                        $modalInstance.close();
                        successloadalert();
                    });

                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                catid: function() {
                    return catid
                }
            }
        });
    }

    $scope.addcategory = function() {
        // console.log("modal");
        var modalInstance = $modal.open({
            templateUrl: 'pagecategoryAdd.html',
            controller: function($scope, $modalInstance, $state) {
                $scope.ok = function(category) {
                    Managepagescategory.add(category, function(){
                        paginate(10,1,$scope.keyword);
                        $scope.alerts = [{ type: 'success', msg: 'Category successfully Added!' }];
                        // $modalInstance.dismiss('cancel');
                    });
                };
                $scope.closeAlert = function (index) {
                    $scope.alerts.splice(index, 1);
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.editcategory = function(mem) {
        var modalInstance = $modal.open({
            templateUrl: 'pagecategoryEdit.html',
            controller: function($scope, $modalInstance, $state, mem) {
                $scope.category = {
                    title: mem.title,
                    subtitle: mem.subtitle,
                    color: mem.colorlegend,
                    metatitle: mem.metatitle,
                    metadesc: mem.metadesc,
                    metakeyword:mem.metakeyword,
                    id:mem.id
                }
                $scope.closeAlert = function (index) {
                    $scope.alerts.splice(index, 1);
                };
                $scope.ok = function(category) {
                    Managepagescategory.edit(category,function(data){
                        paginate(10,1,$scope.keyword);
                        $scope.alerts = [{ type: 'success', msg: 'Category successfully Edited!' }];
                        // $modalInstance.dismiss('cancel');
                    });
                    // console.log(category);
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                mem: function() {
                    return mem
                }
            }
        });
    }

    

})