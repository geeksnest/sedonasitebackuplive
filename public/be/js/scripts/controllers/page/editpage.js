'use strict';

/* Controllers */

app.controller('Editpage', function($scope, $state, Upload ,$q, $http, Config, $stateParams, $interval, $modal, $anchorScroll){

    $scope.imageloader=false;
    $scope.imagecontent=true;

    var categorylist = function()
    {
        $http({
            url: Config.ApiURL + "/pages/listpagecategory",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) { 
            // console.log("success"); 
            loadpageedit();          
            $scope.catlist = data;
        }).error(function (data) {
            $scope.status = status;
        });

    }

    categorylist();

    var loadpageedit = function(){
        $http({
            url: Config.ApiURL + "/page/pageedit/" + $stateParams.pageid ,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.page = data;
            $scope.page.leftbar = data.leftbaritem.leftbar;

            

            if(data.layout == 2)
            {
                $scope.sidebar = true;
            }
            else
            {
                $scope.sidebar = false;
            }
        }).error(function (data, status, headers, config) {
            $scope.status = status;
        });


         $http({
            url: Config.ApiURL + "/page/pagerightitem/" + $stateParams.pageid ,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {

            for (var i = 0; i < data.length; i++) 
                {
                    if(data[i].rightbar == 1)
                    {
                        $scope.r1 = true;
                        rightbarlist.push('1');
                        $.each(rightbarlist, function(i, el){
                        if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
                        });
                    }
                    else if (data[i].rightbar == 2)
                    {
                        $scope.r2 = true;
                        rightbarlist.push('2');
                        $.each(rightbarlist, function(i, el){
                        if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
                        });
                    }
                    else if (data[i].rightbar == 3)
                    {
                        $scope.r3 = true;
                        rightbarlist.push('3');
                        $.each(rightbarlist, function(i, el){
                        if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
                        });
                    }
                    else if (data[i].rightbar == 4)
                    {
                        $scope.r4 = true;
                        rightbarlist.push('4');
                        $.each(rightbarlist, function(i, el){
                        if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
                        });
                    }
                    else if (data[i].rightbar == 5)
                    {
                        $scope.r5 = true;
                        rightbarlist.push('5');
                        $.each(rightbarlist, function(i, el){
                        if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
                        });
                    }
                }

        }).error(function (data, status, headers, config) {
            $scope.status = status;
        });
    }


    $scope.onpagetitle = function convertToSlug(Text)
    {

        if(Text == null)
        {
         
        }
        else
        {
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.page.pageslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
        
    }

    $scope.radio = function(text)
    {
        if(text == 1)
        {
            $scope.sidebar = false;
        }
        else
        {
             $scope.sidebar = true;
        }
    }

    var rightbarlist = [];
    var rightbarlistfinal = [];
   
    $scope.rightbar = function(text)
    {

        

        if($scope.r1 == true)
        {
            rightbarlist.push('1');
            $.each(rightbarlist, function(i, el){
            if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
            });
        }
        if($scope.r1 == false)
        {
            for(var i in rightbarlistfinal){
                if(rightbarlistfinal[i]=='1'){
                    rightbarlistfinal.splice(i,1);
                    break;
                }
            }
        }
        if($scope.r2 == true)
        {
            rightbarlist.push('2');
            $.each(rightbarlist, function(i, el){
            if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
            });
        }
        if($scope.r2 == false)
        {
            for(var i in rightbarlistfinal){
                if(rightbarlistfinal[i]=='2'){
                    rightbarlistfinal.splice(i,1);
                    break;
                }
            }
        }
        if($scope.r3 == true)
        {
            rightbarlist.push('3');
            $.each(rightbarlist, function(i, el){
            if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
            });
        }
        if($scope.r3 == false)
        {
            for(var i in rightbarlistfinal){
                if(rightbarlistfinal[i]=='3'){
                    rightbarlistfinal.splice(i,1);
                    break;
                }
            }
        }
        if($scope.r4 == true)
        {
            rightbarlist.push('4');
            $.each(rightbarlist, function(i, el){
            if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
            });
        }
        if($scope.r4 == false)
        {
            for(var i in rightbarlistfinal){
                if(rightbarlistfinal[i]=='4'){
                    rightbarlistfinal.splice(i,1);
                    break;
                }
            }
        }

        if($scope.r5 == true)
        {
            rightbarlist.push('5');
            $.each(rightbarlist, function(i, el){
            if($.inArray(el, rightbarlistfinal) === -1) rightbarlistfinal.push(el);
            });
        }
        if($scope.r5 == false)
        {
            for(var i in rightbarlistfinal){
                if(rightbarlistfinal[i]=='5'){
                    rightbarlistfinal.splice(i,1);
                    break;
                }
            }
        }
      
    }


    $scope.savePage = function(page)
    {

        page['rightbar'] = rightbarlistfinal;

        $scope.alerts = [];


        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.isSaving = true;

        $http({
            url: Config.ApiURL + "/pages/saveeditedpage",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(page)
        }).success(function (data, status, headers, config) {
            $scope.isSaving = false;
            $scope.alerts.push({type: 'success', msg: 'Page successfully edited!'});
        }).error(function (data, status, headers, config) {
            scope.alerts.push({type: 'danger', msg: 'Something went wrong Page not saved!'});
        });
        $anchorScroll();
    }


    var loadimages = function() {

        $http({
            url: Config.ApiURL + "/pages/listimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            $scope.imagelist = data;
        }).error(function(data) {
            $scope.status = status;
        });
    }

    loadimages();


    $scope.$watch('files', function () {
        $scope.upload($scope.files);
        
    });



    $scope.alertss = [];

        $scope.closeAlerts = function (index) {
            $scope.alertss.splice(index, 1);
        };

    $scope.upload = function (files) 
    {

        
        
        var filename
        var filecount = 0;
        if (files && files.length) 
        {
            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 2000000)
                    {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;
                        
                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                        
    
                    }
                    else

                    {
                        
                   

                        var promises;
                            
                            promises = Upload.upload({
                                
                                url:Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/pageimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    $http({
                                        url: Config.ApiURL + "/pages/saveimage",
                                        method: "POST",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        data: $.param(fileout)
                                    }).success(function (data, status, headers, config) {
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                        
                                    }).error(function (data, status, headers, config) {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                    });
                                    
                                });


                 }


                            
            }
        }
        
          
    };

    $scope.deletepagesimg = function (dataimg)
    {
        // console.log("deleteimg");
        var fileout = {
            'imgfilename' : dataimg
        };

        $http({
            url: Config.ApiURL + "/pages/deletepagesimg",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(fileout)
        }).success(function (data, status, headers, config) {
            loadimages();
        }).error(function (data, status, headers, config) {
            loadimages();
        });

    }

    $scope.cutlink = function convertToSlug(Text, type)
    {
        var texttocut = Config.amazonlink + '/uploads/pageimage/';
        if (type == 'banner'){
            $scope.page.banner = Text.substring(texttocut.length);
        }else if(type=='thumb'){
            $scope.page.imagethumb = Text.substring(texttocut.length);
        }

    }


    var savepagecategoryCTRL = function($scope, $modalInstance, $state) {
        $scope.ok = function(category) {

            $http({
                url: Config.ApiURL + "/pages/createpagecategory",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(category)
            }).success(function (data, status, headers, config) {                
                categorylist();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
                console.log("error");
            });
            
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };  
    }

    $scope.addcategory = function() {
        // console.log("modal");
        
        var modalInstance = $modal.open({
            templateUrl: 'pagecategoryAdd.html',
            controller: savepagecategoryCTRL,
            
        });
    }
    

})