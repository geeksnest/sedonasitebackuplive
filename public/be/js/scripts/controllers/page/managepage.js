'use strict';

/* Controllers */

app.controller('Managepage', function($scope, $state ,$q, $http, Config, $log, Managepages, $interval, $modal){
    $scope.keyword=null;
    $scope.loading = false;
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    var paginate = function(num, off, keyword){
        $scope.loading = true;
        Managepages.sample(num,off, keyword, function(data){
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.loading = false;
        });
    }

    paginate(num, off, keyword);

    $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        paginate(10, 1, keyword);
        $scope.searchtext = '';
    }

    $scope.setPage = function (pageNo) {
        paginate(10, pageNo, $scope.keyword);
    };


    $scope.setstatus = function (status,pageid,keyword) {
        var newstat;
        if(status == 1)
        {
            newstat = 0;
        }
        else
        {
            newstat = 1;
        }
        Managepages.updatestatus(pageid,$scope.keyword, newstat, function(data){
            $scope.currentstatusshow = pageid;
            var i = 2;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    paginate(10, 1, $scope.keyword);
                    $scope.currentstatusshow = 0;
                }
            },1000)
        });
    }

    $scope.deletepage = function(pageid) {

        var modalInstance = $modal.open({
            templateUrl: 'pageDelete.html',
            controller: function($scope, $modalInstance, pageid) {
                $scope.ok = function() {
                    Managepages.delete(pageid, function(data){
                        paginate(10, 1, $scope.keyword);
                        $modalInstance.close();
                        successloadalert();
                    })
                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                pageid: function() {
                    return pageid
                }
            }
        });
    }

    var successloadalert = function(){
            $scope.alerts = [{ type: 'success', msg: 'Page successfully Deleted!' }];
    }

    var errorloadalert = function(){
            $scope.alerts = [{ type: 'danger', msg: 'Something went wrong Page not Deleted!' }];
    }

    $scope.clear = function(){
        $scope.keyword=null;
        paginate(10, 1, $scope.keyword);
    }

    $scope.editpage = function(pageid) {
        $scope.newsid
        var modalInstance = $modal.open({
            templateUrl: 'pageEdit.html',
            controller: function($scope, $modalInstance, pageid, $state) {
                $scope.pageid = pageid;
                $scope.ok = function(pageid) {
                    $scope.pageid = pageid;
                    $state.go('editpage', {pageid: pageid });
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                pageid: function() {
                    return pageid
                }
            }
        });
    }

    

})