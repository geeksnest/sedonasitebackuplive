app.controller('AddUserCtrl', function($scope, $http, User, Config, Upload, $anchorScroll, $modal, $filter, $timeout) {
    //VALIDATE USERNAME
    $scope.validusrname = false;
    $scope.validemail = false;
    $scope.pwdconfirm = false;
    $scope.base_url = Config.BaseURL;
    $scope.process = 0;
    var userid = null;

    $scope.chkusername = function(username) {
        User.usernameExists(username, userid, function(data) {
            $scope.validusrname = data.exists;
        });
    };


    //VALIDATE EMAIL
    $scope.chkemail = function(email) {
        User.emailExists(email, userid, function(data) {
            $scope.validemail = data.exists;
        });
    };
    //VALIDATE PASSWORD length
    $scope.chkpass = function(password, conpass) {
        if (password.length >= 6) {
            $scope.pwd = false;
        } else {
            if (conpass !== undefined && conpass == password) {
                $scope.pwdconfirm = true;
            } else {
                $scope.pwdconfirm = false;
            }
            $scope.pwd = true;
        }
        console.log($scope.pwdconfirm);
    };
    //VALIDATE PASSWORD IF MATCH
    $scope.confirmpass = function() {
        var password = $('#password').val();
        var confirmpass = $scope.user.conpass;
        if (password != confirmpass) {
            $scope.pwdconfirm = true;
        } else {
            $scope.pwdconfirm = false;
        }
    };

    $scope.prepare = function(file) {
        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        if (file && file.length) {
            if (file.size >= 2000000) {
                console.log('File is too big!');
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'File ' + file.name + ' is too big'
                }];
                $scope.file = '';
            } else {
                console.log("below maximum");
                $scope.file = file;
                $scope.closeAlert();
            }
        }
    }
    //SAVE DATA
    $scope.submitData = function(user, file) {
        $scope.alerts = [];
        var x = 1;
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        if (file && file.length) {

            if (file.size >= 2000000) {
                console.log('File is too big!');
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'File ' + file.name + ' is too big'
                }];
                $scope.file = '';
            } else {
                User.uploadpic(file, function(log) {
                    console.log(log);
                    $scope.process = log;
                    if (log == 100 && x == 1) {
                        x = 2;
                        user.profpic = file[0].name;
                        user.bday = $filter('date')(user.bday, 'yyyy-MM-dd');

                        User.register(user, function(data) {
                            console.log(data);
                            $scope.file = undefined;
                            $scope.alerts.push({
                                type: data.type,
                                msg: data.msg
                            });
                            $scope.userform.$setPristine();
                            $scope.process = 0;
                            $('input').val(undefined);
                        });

                        $anchorScroll();
                    }
                });
            }
        }
    }


    //DATE PICKER
    $scope.today = function() {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function() {
        $scope.dt = null;

    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

});