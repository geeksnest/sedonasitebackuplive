app.controller('UserListCtrl', function($http, $scope, $stateParams, $modal, $sce, Config, User) {
  //LIST ALL USERS
  var num = 10;
  var off = 1;
  var keyword = null;
	$scope.loading = false;
  var adminid = $stateParams.userid;

  var paginate = function(off, num, keyword) {
		$scope.loading = true;
    User.list(num, off, keyword, adminid, function(data) {
      $scope.data = data;
      $scope.total_items = data.total_items;
      if (keyword !== undefined && off === 0) {
        $scope.keywordshow = true;
        $scope.keyword = keyword;
      }
			$scope.maxSize = 10;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
			$scope.loading = false;
    });
  };

  $scope.search = function(keyword) {
    if (keyword === "") {
      keyword = undefined;
    }
    var off = 0;
    paginate(off, num, keyword);
  };

  $scope.clear = function() {
    paginate(off, num, undefined);
    $scope.keywordshow = false;
  };
  $scope.setPage = function(off, keyword) {
    paginate(off, keyword);
  };

  paginate(off, keyword);
  //END USER LISTING


  //DELETE USER
  $scope.delete = function(user) {
    var modalInstance = $modal.open({
      templateUrl: 'userDelete.html',
      controller: dltCTRL,
      resolve: {
        dltuser: function() {
          return user;
        }
      }
    });
  };
  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  var alertme = function(data) {
    console.log(data);
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({
      type: data.type,
      msg: data.msg
    });
  };
  var dltCTRL = function($scope, $modalInstance, dltuser) {
    $scope.dltuser = dltuser;

    $scope.ok = function(dltuser) {
      User.delete(dltuser, function(data) {
        paginate(off, keyword);
        $modalInstance.dismiss('cancel');
        alertme(data);
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };
  //UPDATE USER
  $scope.update = function(user) {
    var modalInstance = $modal.open({
      templateUrl: 'userUpdate.html',
      controller: updateCTRL,
      resolve: {
        update: function() {
          return user;
        }
      }
    });
  };
  var updateCTRL = function($scope, $modalInstance, update, $state) {
    $scope.update = update;
    $scope.ok = function(update) {
      $state.go('updateuser', {
        userid: update
      });
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.setstatus = function(status, id, keyword) {
    var newstat;
    if (status == 1) {
      newstat = 0;
    } else {
      newstat = 1;
    }

    User.updatestatus(id, newstat, function(data) {
      $scope.currentstatusshow = id;
      var i = 2;
      setInterval(function() {
        i--;
        if (i == 0) {
          paginate(10, 1, keyword);
          $scope.currentstatusshow = 0;
        }
      }, 1000);
    });
  };

});
