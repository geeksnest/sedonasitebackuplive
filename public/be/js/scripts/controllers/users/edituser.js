'use strict';

app.controller('UserUpdateCtrl', function ($http, $scope, $stateParams, $modal,$sce, Config, User, $filter, $anchorScroll) {
	console.log($stateParams.userid);
	$scope.process = 0;
	$scope.amazonlink = Config.amazonlink;

	var info = function(){
		User.info($stateParams.userid, function(data) {
			$scope.user = data;
			$scope.user.userrole = data.task;
			if(data.status == 1){
				$scope.user.status = true;
			}else {
				$scope.user.status = false;
			}
			console.log($scope.user);
		});
	}

	info();

	//VALIDATE USERNAME
    $scope.chkusername = function(username) {
        User.usernameExists(username, $stateParams.userid, function(data) {
            $scope.validusrname = data.exists;
            console.log(data.exists);
        });
    };


    //VALIDATE EMAIL
    $scope.chkemail = function(email) {
        User.emailExists(email, $stateParams.userid, function(data) {
            $scope.validemail = data.exists;
        });
    };
    
	$scope.prepare = function(file){
		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
		if (file && file.length) {
			if(file.size >= 2000000){
	            console.log('File is too big!');
	            $scope.alerts = [{type: 'danger', msg: 'File ' + file.name + ' is too big'}];
	            $scope.file='';
	        }else {
				console.log("below maximum");
				$scope.file = file;
				$scope.closeAlert();
	        }
		}
	}

	$scope.updateData = function(user){
		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
		var x = 1;
		
		user.bday = $filter('date')(user.bday,'yyyy-MM-dd');
		if(user.profile_pic_name.length == 1){
			var file = user.profile_pic_name;
			user.profile_pic_name = user.profile_pic_name[0].name; 

			if (file && file.length) {

	            if (file.size >= 2000000){
	                console.log('File is too big!');
	                $scope.alerts = [{type: 'danger', msg: 'File ' + file.name + ' is too big'}];
	                $scope.file='';
                    $scope.userform.$setPristine();
					$anchorScroll();
	            }else{
	            	User.uploadpic(file, function(log) {
	            		$scope.process = log;
		            	if(log == 100 && x == 1){
		            		x = 2;
			                user.bday = $filter('date')(user.bday,'yyyy-MM-dd');

			               User.update(user, function(data){
				                $scope.alerts.push({type: data.type, msg: data.msg });
				                $scope.process = 0;
								$anchorScroll();
								angular.element("#profpic").attr("ngf-default-src", "{[{amazonlink}]}/uploads/userimages/{[{user.profile_pic_name}]}");
							});
		            	}
	            	});
	            }
			}
		}else {
			User.update(user, function(data){
                $scope.alerts.push({type: data.type, msg: data.msg });
				$anchorScroll();
                $scope.userform.$setPristine();
			});
		}

	}

	//DATE PICKER
	$scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
});