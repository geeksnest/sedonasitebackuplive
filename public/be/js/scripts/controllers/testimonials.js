'use strict';

/* Controllers */

app.controller('TestimonialsCTRL', function($scope, $state ,$q, $http, Config, $timeout, $modal, Testimonials, $filter, Managepages){


    $scope.keyword=null;
    $scope.loading = false;
    $scope.base_url = Config.BaseURL;
    $scope.alerts = [];
    $scope.am = Config.amazonlink;

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.editcategoryshow = false;


    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;

    var paginate = function(num, off, keyword){
        $scope.loading = true;
        Testimonials.paging(num,off, keyword, function(data){
            $scope.data = data;
            $scope.loading = false;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    paginate(num, off,keyword);

    $scope.search = function (keyword) {
        var off = 1;
        $scope.keyword = keyword;
        paginate(num, 1, $scope.keyword);
        $scope.searchtext = '';
    }

    $scope.clear = function(){
        $scope.keyword=null;
        paginate(0, 1,null);
    }

    $scope.setPage = function (pageNo) {
        paginate(num, pageNo,$scope.keyword);
    };


    // Modals

    //Add modal
    $scope.addtesti = function() {
        var modalInstance = $modal.open({
            templateUrl: 'addTestimonial.html',
            controller: function($scope, $modalInstance, $state) {
                $scope.file='';
                $scope.saving=false;
                $scope.alerts=[];

                $scope.closeAlert = function(index) {
                    $scope.alerts.splice(index, 1);
                };

                $scope.pages='';

                $scope.testi = {};

                var oritesti = angular.copy($scope.testi);

                $scope.resetForm = function ()
                {
                    $scope.testi = angular.copy(oritesti);
                    //$scope.testipage.$setPristine();
                    $scope.file='';
                };


                Managepages.get(function(pages){
                    $scope.pages = pages;
                });

                $scope.ok = function(testi) {
                    $scope.saving = true;
                    Testimonials.uploadprofile($scope.file, function(filename){
                        testi.files = filename;
                        testi.dt = $filter('date')(testi.dt,'yyyy-MM-dd');
                        Testimonials.create(testi, function(data) {
                            console.log(data);
                            $scope.saving = false;
                            $scope.resetForm();
                            $scope.alerts = [{type: 'success', msg: 'Successfully added testimonial'}];
                        });
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.prepare = function (files){
                    if (files && files.length) {
                        files.map(function(file){
                            if (file.size >= 2000000){
                                console.log('File is too big!');
                                $scope.alerts = [{type: 'danger', msg: 'File ' + file.name + ' is too big'}];
                                $scope.file='';
                            }else{
                                $scope.file = files;
                                $scope.closeAlert();
                            }
                        });
                    }
                }
            }
        }).result.finally(function(){
                paginate(10,1,$scope.keyword);
            });;
    }
    // end add testi modal

    // Edit Testi modal


    $scope.editTesti = function(testiId) {
        var modalInstance = $modal.open({
            templateUrl: 'testiEdit.html',
            controller: function($scope, $modalInstance, testiId, $state) {
                $scope.file='';
                $scope.saving=false;
                $scope.alerts=[];
                $scope.closeAlert = function(index) {
                    $scope.alerts.splice(index, 1);
                };;

                $scope.am = Config.amazonlink;

                Managepages.get(function(pages){
                    $scope.pages = pages;
                });

                Testimonials.getTesti(testiId, function(data){
                    var test = data[0];
                    $scope.testi = {
                        'name': test.name,
                        'message': test.message,
                        'page': test.pageid,
                        'dt': test.date,
                        'picture': test.picture
                    };
                })

                $scope.prepare = function (files){
                    if (files && files.length) {
                        files.map(function(file){
                            if (file.size >= 2000000){
                                console.log('File is too big!');
                                $scope.alerts = [{type: 'danger', msg: 'File ' + file.name + ' is too big'}];
                                $scope.file='';
                            }else{
                                $scope.file = files;
                                $scope.closeAlert();
                            }
                        });
                    }
                }

                $scope.ok = function(testi) {
                    $scope.saving = true;
                    Testimonials.uploadprofile($scope.file, function(filename){
                        testi.files = filename;
                        testi.dt = $filter('date')(testi.dt,'yyyy-MM-dd');
                        testi.id = testiId;
                        Testimonials.update(testi, function(data) {
                            console.log(data);
                            $scope.saving = false;
                            if (filename != '') {
                                $scope.testi.picture = filename;
                            }
                            $scope.alerts = [{type: 'success', msg: 'Successfully updated testimonial!'}];
                            $modalInstance.dismiss('cancel');
                        });
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                testiId: function() {
                    return testiId
                }
            }
        }).result.finally(function(){
                paginate(10,1,$scope.keyword);
            });
    }

    // end testi modal

    // Delete testi modal

    $scope.deletetesti = function(testiId) {

        var modalInstance = $modal.open({
            templateUrl: 'testiDelete.html',
            controller: function($scope, $modalInstance, testiId) {
                $scope.msg='Are you sure you want to delete this testimonial?';
                $scope.showbuttons = true;
                $scope.ok = function() {
                    Testimonials.delete(testiId, function(result){
                        if(result.hasOwnProperty('success')){
                            $scope.msg='Testimonial has been deleted.';
                            $timeout(function(){
                                $scope.cancel();
                            }, 2000);
                        }else{
                            $scope.msg='Error occured.';
                            $timeout(function(){
                                $scope.cancel();
                            }, 2000);
                        }
                        $scope.showbuttons = false;
                    });
                };

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };


            },
            resolve: {
                testiId: function() {
                    return testiId
                }
            }
        }).result.finally(function(){
        paginate(10,1,$scope.keyword);
            });
    }

})
