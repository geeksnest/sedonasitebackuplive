'use strict';

/* Controllers */

app.controller('Editauthor', function($scope, $state, Upload ,$q, $http, Config, Createauthor, $stateParams ,$modal, $anchorScroll){

    $scope.imageloader=false;
    $scope.imagecontent=true;

    $scope.author = {
      name: ""
    };

    var oriauthor = angular.copy($scope.author);


    // console.log(authorid);
    $http({
        url: Config.ApiURL + "/news/editauthor/" + $stateParams.authorid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        $scope.author = data;
        
    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });


    $scope.cutlink = function convertToSlug(Text)
    {
        var texttocut = Config.amazonlink + '/uploads/saveauthorimage/';
        $scope.author.photo = Text.substring(texttocut.length); 
    }


    $scope.updateAuthor = function(author)
    {
        
        $scope.alerts = [];

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.isSaving = true;
        $http({
            url: Config.ApiURL + "/news/edieauthor",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(author)
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.isSaving = false;
            $scope.alerts.push({type: 'success', msg: 'Author successfully updated!'});
            $anchorScroll();
        }).error(function (data, status, headers, config) {
            scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
        });
     
    }

    var loadimages = function() {

        $http({
            url: Config.ApiURL + "/news/authorlistimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            $scope.imagelist = data;
        }).error(function(data) {
            $scope.status = status;
        });
    }

    loadimages();


    $scope.$watch('files', function () {
        $scope.upload($scope.files);
        
    });



    $scope.alertss = [];

        $scope.closeAlerts = function (index) {
            $scope.alertss.splice(index, 1);
        };

    $scope.upload = function (files) 
    {

        
        
        var filename
        var filecount = 0;
        if (files && files.length) 
        {
            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 2000000)
                    {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;
                        
                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                        
    
                    }
                    else

                    {
                        
                   

                        var promises;
                            
                            promises = Upload.upload({
                                
                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/saveauthorimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    $http({
                                        url: Config.ApiURL + "/news/saveauthorimage",
                                        method: "POST",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        data: $.param(fileout)
                                    }).success(function (data, status, headers, config) {
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                        
                                    }).error(function (data, status, headers, config) {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                    });
                                    
                                });


                 }


                            
            }
        }
        
          
    };
   

    $scope.deleteauthorimg = function (dataimg)
    {
        var fileout = {
            'imgfilename' : dataimg
        };

        $http({
            url: Config.ApiURL + "/news/deleteauthorimg",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(fileout)
        }).success(function (data, status, headers, config) {
            loadimages();
        }).error(function (data, status, headers, config) {
            loadimages();
        });

    }

    
 

})
