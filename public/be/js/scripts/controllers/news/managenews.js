'use strict';

/* Controllers */

app.controller('Managenews', function($scope, $state ,$q, $http, Config, $log, Managenews, $interval, $modal, $sce, $stateParams){
    $scope.keyword=null;
    $scope.loading = false;
    // $scope.vidpath = $scope.video.video;
    $scope.sort = "dateedited";

    $scope.paste = function(video){
        $scope.vidpath=$sce.trustAsHtml($scope.video.video);
        console.log("video");
    }


    var successloadalertvideo = function(){
        $scope.alerts.push({ type: 'success', msg: 'Featured Video successfully Updated.' });

    }

    var errorloadalertvideo = function(){
        $scope.alerts.push({ type: 'danger', msg: 'Something went wrong saving the video!' });
    }

    $scope.sortpage = function(sort){
        loadlist(num, off, keyword, sort);
        console.log(sort);
    }

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    var loadlist = function(num, off, keyword, sort){
        $scope.loading = true;
        Managenews.list(num,off, keyword, sort, function(data){
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.loading = false;
        });
    }

    loadlist(num, off, keyword, $scope.sort);

    $scope.clear = function(){
        $scope.keyword=null;
        loadlist(num, off, keyword, $scope.sort);
    }

    $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        loadlist(num, off, keyword, $scope.sort);
        $scope.searchtext='';
        $scope.formmanagenews.$setPristine();
    }

    $scope.setPage = function (pageNo) {
        loadlist(num, pageNo, keyword, $scope.sort);
    };

    $scope.setstatus = function (status,newsid,keyword,newslocation,newsslugs) {

        $scope.currentstatusshow = newsslugs;
        var newstat;
        if(status==1){
            newstat = 0;
        }else{
            newstat = 1;
        }
        Managenews.updateNewsTags(newstat,newsid,keyword, function(data){
            var i = 2;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    loadlist(10, 1, keyword, $scope.sort);
                    $scope.currentstatusshow = 0;
                }
            },1000)

        });

    }
  
    $scope.deletenews = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsDelete.html',
            controller: function($scope, $modalInstance, newsid) {
                $scope.ok = function() {
                    Managenews.delete(newsid, function(data){
                        loadlist(10, 1, keyword, "dateedited");
                        $modalInstance.close();
                        successloadalert();
                    });
                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };

            },
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }


    var successloadalert = function(){
            $scope.alerts = [{ type: 'success', msg: 'News successfully Deleted!' }];
            
    }

    var errorloadalert = function(){
            $scope.alerts = [{ type: 'danger', msg: 'Something went wrong News not Deleted!' }];
    }


     $scope.deletenewscenter = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsCenterDelete.html',
            controller: newsCenterDeleteCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

    $scope.editnews = function(newsid) {
        $scope.newsid
        var modalInstance = $modal.open({
            templateUrl: 'newsEdit.html',
            controller: function($scope, $modalInstance, newsid, $state) {
                $scope.newsid = newsid;
                $scope.ok = function(newsid) {
                    $scope.newsid = newsid;
                    $state.go('editnews', {newsid: newsid });
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

})