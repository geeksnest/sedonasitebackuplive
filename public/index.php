<?php

error_reporting(E_ALL);
ini_set("display_errors", 0);
try {

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();

	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);


		$router->add('/:controller/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'page404',
			'params' => 3
		));

		$router->add('/:controller', array(
			'module' => 'frontend',
			'controller' => 1
		));

		$router->add('/blog/:action/:params/:params', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 1,
			'params' => 2,
			'params' => 3
		));

		$router->add('/:action', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 1
		));

		$router->add('/maintenance', array(
			'module' => 'frontend',
			'controller' => "maintenance"
		));

		$router->add('/blog/{newsslugs}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'view'
		));

		$router->add('/blog/all', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'all'
		));

		$router->add('/blog/category/{category}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'category'
		));

		$router->add('/blog/tags/{tags}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'tags'
		));

		$router->add('/blog/author/{id}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'author'
		));

		$router->add('/blog/archive/{month}/{date}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'archive'
		));

		$router->add('/healing/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
		));
		$router->add('/readings/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
		));
		$router->add('/acupuncture/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
		));
		$router->add('/retreats/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
		));
		$router->add('/workshops/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
		));
		// $router->add('/page/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'index'
		// ));
		// //
		// $router->add('/{sample}/healing/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'index'
		// ));
		// $router->add('/{sample}/readings/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'index'
		// ));
		// $router->add('/{sample}/acupuncture/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'index'
		// ));
		// $router->add('/{sample}/retreats/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'index'
		// ));
		// $router->add('/{sample}/workshops/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'index'
		// ));
		// //
		$router->add('/', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'index'
		));
		$router->add('/sedonaadmin', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/sedonaadmin/:controller', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 'index'
		));

		$router->add('/sedonaadmin/:controller/:action', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 3
		));

		$router->add('/sedonaadmin/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3,
		));

		$router->removeExtraSlashes(true);

		return $router;
	};

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
	    return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
	$di->set('modelsManager', function() {
	      return new Phalcon\Mvc\Model\Manager();
	});
    $config = include "../app/config/config.php";
    // Store it in the Di container
    $di->set('config', function () use ($config) {
        return $config;
    });
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
		)
	));

	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
