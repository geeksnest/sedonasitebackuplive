<?php header("Content-Type: application/xml; charset=ISO-8859-1"); ?>
<rss version="2.0">
  <channel>
    <title>RSS Title</title>
    <description>Sedona Healing Arts' RSS feed</description>
    <link><?php echo $base_url; ?>/feed/index</link>
    <lastBuildDate><?php echo date('D, d M Y H:i:s O'); ?></lastBuildDate>
    <?php foreach($items as $key => $value){?>
      <item>
        <title><![CDATA[ <?php echo $items[$key]->title; ?> ]]></title>
        <pubDate><?php echo date('F d, Y',strtotime($items[$key]->date)); ?></pubDate>
        <link><?php echo $base_url."/blog/".$items[$key]->newsslugs; ?></link>
        <description><![CDATA[ <?php echo substr(strip_tags($items[$key]->body), 0 ,400)."..."; ?> ]]></description>
      </item>
    <?php } ?>
  </channel>
</rss>
