
    <!-- Banner starts -->
    <div class="banner-container retreats-bg retreats-bg-manifest-your-dream">
        <div class="black-box">
            <span class="banner-title">Manifest your dream</span>
            <br/>
            <span class="banner-sub-title1">- 5 or 6 DAYS -</span>
            <br/>
            <span class="banner-sub-title1">Transform Your Dream into a Reality</span> <br/><br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">SCHEDULE YOUR RETREAT</a></div>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="col-sm-8 no-margin content-text">

                <div class="phar no-padding">
                    <span class="size20"><h2>MANIFEST YOUR DREAM</h2></span>
                      <p class="size18">
                          The Manifest Your Dream Retreat will empower the confidence, passion, drive, and focus required in order to transform your dreams into realities.  True happiness occurs when you actualize the things that you want deep in your heart.  This retreat will revive your purest spirit while instilling powerful, simple, practical techniques that will enable you to create your life exactly as you wish.
                      </p>

                    <p class="size20 title-like-header">The Retreat Includes</p>
                    <div class="padding-left20">
                        <h3>Intuitive Reading and Guidance</h3>
                        <p class="">
                            Key information is read from the highest and most complete level of the soul and distilled into everyday language so that you may understand (from a higher perspective) why you experience the struggles that you face.  You may discuss any topic during your session; past or current issues, relationships, work and career, life purpose and spiritual growth, physical health, mental health, emotional health, finances, etc. You may also ask for insight into any person who may be a part of your life; past, present, or future.  Intuitive readings are a powerful tool to bring clarity where there was once confusion, certainty where there was once doubt, and triumph where there were once obstacles.
                        </p>
                        <h3>Customized Energy Training Techniques</h3>
                        <p class="">
                            A customized Mind/Body Energy Training program will be developed based upon your specific physical, mental, emotional, and spiritual needs.  You will receive training on meridian opening exercises and self-healing energy techniques that will help you reconnect with the essence of your soul while in Sedona, as well as help you maintain the benefits of the retreat long after you have returned home.
                        </p>
                        <h3>Private Guided Meditation (Choose One)</h3>
                        <div class="padding-left20">
                            <h3>A. Breathing Meditation</h3>
                            <p>
                                Experience deep relaxation of mind, body and energy through various meditative breathing postures.
                            </p>
                            <h3>B. Ki-Gong Meditation</h3>
                            <p>
                                Gentle movements following ki energy will guide you to a state of relaxed concentration in this moving meditation.
                            </p>
                        </div>
                        <h3>Vortex Hike and Guided Meditation</h3>
                        <p class="">
                            Experience Sedona’s powerful vortex energy for yourself with a guided meditation that will help you feel the energies as they cleanse and heal your body, relax your mind, and calm your spirit.
                        </p>
                        <h3>Wooden Pillow Self-Healing Class</h3>
                        <p class="">
                            Learn easy yet profound self-healing techniques using a wooden pillow. You will experience the immediate benefits of this self-healing in your spine, neck and back.
                        </p>
                        <h3>Private Healing (Choose One)</h3>
                        <div class="padding-left20">
                            <h3>A. Chakra Balancing and Crystal Healing</h3>
                            <p>
                                The Chakra Balancing and Crystal Healing Session utilizes the natural resonance of crystals to recalibrate a healthy circulation of vital life-force energy through the chakras, which are the seven energy centers that run along the length of the spine, starting at the tailbone and ending at the crown.  Known in eastern medicine as the spiritual organs, each chakra is associated with the varying emotions and attitudes that we experience in day-to-day life.  When all seven chakras are healthy and balanced you’ll feel physically, energetically, and spiritually vibrant. Conversely, when the energy flow is decreased, either through a blockage or weakness, unexplained fatigue, pain, negative thoughts and emotions, or disease may manifest. Through The Chakra Balancing & Crystal Healing, you’ll recover a healthy energy balance of the chakras, leaving you feeling deeply relaxed, refreshed, hopeful, and renewed.
                            </p>
                            <h3>B. Life Particle Energy Balancing</h3>
                            <p>
                                Life Particles are a concept of energy that represents the most essential form of life as well as our being. Life Particles are matter, energy, and consciousness combined, and each and every person is made up of these tiny elementary particles. Life Particles are a perfect form of energy that never lose their balance and purity. Unfortunately, due to various negative or harmful experiences and memories, we lose our natural energetic balance. In such cases, receiving Life Particles will help you recover your energetic, emotional, and spiritual balance. Life Particle Energy Healers at Sedona Healing Arts are thoroughly trained in Life Particles meditation and deliver to you this purest form of energy.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="phar no-padding">
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size16 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>MANIFEST YOUR DREAM</h2>
                    <h1>5 to 6 Day Retreat</h1>
                    <h3 class="grayfont">Price: Single $1,600 / Couple $2,995</h3>
                                <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
                                <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->