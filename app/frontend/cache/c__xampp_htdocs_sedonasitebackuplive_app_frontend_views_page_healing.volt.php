
    <!-- Banner starts -->
    <div class="banner-container healing-bg" title="<?php echo $title; ?>">
        <img src="/img/frontend/healing.jpg" class="pinterest-img" alt="<?php echo $title; ?>">
        <div class="black-box">
            <span class="banner-title"><?php echo $healing[0]->cattitle;?></span>
            <br/>
            <?php if($healing[0]->catsubtitle !=''){echo "<span class='banner-sub-title1'>".$healing[0]->catsubtitle."</span><br/>";}?>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="span12 center retreats-section">
                <?php
                          $getginfo = $healing;
                          foreach ($getginfo as $key => $value) {
                ?>
                <div class="span3 marg-bot center landing-page-cols-first hbs">
                    <a href="<?php echo $base_url;?>/healing/<?php echo $getginfo[$key]->pageslugs;?>">
                            <div class="title2 center landing-page-cols-thumb no-padding no-margin" style="background-image:  url('<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>'); " title="<?php echo $getginfo[$key]->imagethumbsubtitle ?>">
                                <img src="<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>" class="pinterest-img" alt="<?php echo $getginfo[$key]->imagethumbsubtitle ?>"/>
                                <div class="imagethumbtitle"><?php echo $getginfo[$key]->title ?></div>
                            </div>
                        </a>
                    <div class="text-phar text-left">
                        <div class="subtitle"><?php echo $getginfo[$key]->imagethumbsubtitle ?></div>
                    <span>
                    <?php
                        echo strip_tags($getginfo[$key]->thumbdesc);
                    ?>
                    <a href="<?php echo $base_url;?>/healing/<?php echo $getginfo[$key]->pageslugs;?>">Learn more.</a>
                    </span>
                    </div>
                    <div class="button2 center"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">Make an Appointment</a></div>
                    <br/>
            </div>
                <?php } ?>
                <div class="clearfix"></div>
        </div>

    </div>


    <!-- Below Banner ends -->
