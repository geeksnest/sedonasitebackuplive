<?php echo $this->getContent()?>
<div class="banner-container blog-bg" title="<?php echo $title; ?>">
  <img src="/img/frontend/blog.jpg" class="pinterest-img" alt="<?php echo $title; ?>">
</div>
<div class="container margin-top40 " ng-controller="NewsCtrl" ng-init="getarchives()">
  <div class="row">
    <div class="col-sm-9">
      <h4 class="blog-title-list"> Archive: {[{ archive }]}</h4>
      <div class="list-news-wrapper">
      <div class="row list-title-blog" ng-repeat="news in newslist" ng-click="redirectNews(news.newsslugs);">

        <!--<div class="col-sm-3 news-thumb-container">-->
          <!--<div class="youtube-play" ng-show="news.videothumb"><img src="/img/youtubeplay.png"/></div>-->
          <!--<a href="/blog/view/{[{ news.newsslugs; }]}">-->
            <!--<img ng-show="news.videothumb" src="{[{ news.videothumb | returnYoutubeThumb }]}" class="news-tumb">-->
            <!--<img ng-show="news.imagethumb" src="{[{ news.imagethumb | returnImageThumb  }]}" class="news-tumb">-->
          <!--</a>-->
        <!--</div>-->

        <div class="col-sm-3 news-thumb-container" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" title="{[{ news.title }]}">
            <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
            <img src="{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
          <div class="youtube-play" ng-show="news.videothumb"><img src="/img/youtubeplay.png"/></div>
          <a href="/blog/view/{[{ news.newsslugs; }]}">
          </a>
        </div>


        <div class="col-xs-8 col-md-7 news-list-desc">
          <div class="row">

              <div class="col-sm-12">
                <span class="size25 font1 news-title">{[{ news.title }]}</span>
              </div>
              <div class="col-sm-12">
                <strong><span class="thin-font1 orange">{[{ news.categorylist }]}</span></strong> <span ng-show="news.name !=''" class="thin-font1"> / by <strong><span class="orange">{[{ news.name }]}</span></strong></span> / {[{ news.date }]}
                <br/><br/>
              </div>
              <div class="col-sm-12">
                <div class="font1 size14 summary">
                  {[{ news.summary }]}
                  <br/><br/>
                </div>
              </div>

              <!-- <div class="col-sm-12 font1 size15">
              <br/>
                <?php
                $removedimg = strip_tags($getginfo[$key]->body, '');
                $limitcontent = substr($removedimg,0,275);
                echo $limitcontent;
                ?>
              </div> -->
          </div>

          <div style="clear:both"></div>
          <br>
        </div>

      </div>
        </div>
      <button ng-hide="hideloadmore" class="news-list-show-more" ng-click="getarchives()" ng-disabled="loading"><span ng-show="loading">Loading</span><span ng-hide="loading">Show More</span></button>
    </div>


    <div class="col-sm-3">
      <div class="no-padding">
        <hr class="styled-hr">
          <span class="size25 font1">About Banya</span>
          <span class="size16 font1"><br/><br/>
          Banya Lim is the spiritual intuitive guide and healer at Sedona Story. She is a nationally licensed acupuncturist and healer of Oriental Medicine. Banya will equip you with new clarity to see your life in a much more positive and hopeful direction.</span>  <br/><br/>
          <!-- <span class="size25 font1">Archives</span> <br/> <br/>
          <span class="size16"><a href="">March 2015 </a></span> <br/> <br/>  -->
          <hr class="styled-hr">
          <span class="size25 font1">Categories</span> <br/>
          <div class="categorylist-wrapper">
            <span class="size16"><a href="<?php echo $base_url;?>/blog/all">All</a></span>
            <?php
              $getginfo = $newscategory;
              foreach ($getginfo as $key => $value) {
            ?>
              <span class="size16"><a href="/blog/category/<?php echo $getginfo[$key]->categoryslugs; ?>"><?php echo $getginfo[$key]->categoryname;  ?></a></span>
            <?php } ?>
          </div>

          <!-- archives -->
          <hr class="styled-hr">
          <h2 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h2>
          <div class="ul-archives">
            <?php
              $getarchives = $archivelist;
              foreach ($getarchives as $key => $value) { ?>
                <a href="<?php echo $base_url; ?>/blog/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"><span class="fa fa-chevron-right"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a>
            <?php
            } ?>
          </div>

          <!-- tags -->
          <hr class="styled-hr">
          <h2 class="font1"><span class="fa fa-tag fa-1x"></span> Tags</h2>
          <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { ?>
              <a href="<?php echo $base_url; ?>/blog/tags/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
          <?php
            $count > 5 ? $count =1 : $count++;
          } ?>

          <hr class="styled-hr"> <br/>
          <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="/img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
      </div>
    </div>

  </div>
  <hr class="styled-hr">
</div>
