<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class BlogController extends ControllerBase
{
    public function indexAction()
    {

        //list Category

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


        //featured news
        $service_url = $this->config->application->ApiURL. '/news/frontend/featurednews';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->featurednews = $decoded;

        //latest news
        $service_url = $this->config->application->ApiURL. '/news/frontend/latest';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->latestnews = $decoded;



    }

    public function viewAction($newsslugs)
    {
        $service_url = $this->config->application->ApiURL. '/news/frontend/view/' . $newsslugs;

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newsid = $decoded[0]->newsid;
        $this->view->title = $decoded[0]->title;
        $this->view->newsslugs = $decoded[0]->newsslugs;
        $this->view->author = $decoded[0]->author;
        $this->view->summary = $decoded[0]->summary;
        $this->view->body = $decoded[0]->body;

        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;

        $this->view->imagethumb = $decoded[0]->imagethumb;

        if($decoded[0]->status == 0){
            $this->response->redirect($this->config->application->BaseURL.'/page404');
        }

        if(!empty($decoded[0]->videothumb)){
            /*$vid = preg_replace("/(width=\"(.*?)\")/", "width=\"100%\"", $decoded[0]->videothumb);
            $vid = preg_replace("/(height=\"(.*?)\")/", "height=\"368px\"", $vid);*/
            $vid = preg_replace("/<iframe /", "<iframe class=\"blog-videos\"", $decoded[0]->videothumb);
            $this->view->featured = $vid;
            $this->view->featuredTitle = "Featured Video";
            $imgthumb = preg_replace("/<iframe width=\"420\" height=\"315\" src=\"https:\/\/www.youtube.com\/embed\//", "", $decoded[0]->videothumb);
            $imgthumb = preg_replace("/<iframe width=\"560\" height=\"315\" src=\"https:\/\/www.youtube.com\/embed\//", "", $imgthumb);
            $this->view->imgthumb = "http://img.youtube.com/vi/".preg_replace("/\" frameborder=\"0\" allowfullscreen><\/iframe>/", "", $imgthumb)."/hqdefault.jpg";
        }elseif(!empty($decoded[0]->imagethumb)){
            $this->view->featured = '<a class="fancybox-effects-a" href="'.$this->config->application->amazonlink .'/uploads/newsimage/'. $decoded[0]->imagethumb.'"><div class="featured-image" style="background-image: url(\''.$this->config->application->amazonlink .'/uploads/newsimage/'. $decoded[0]->imagethumb.'\')"></div></a>';
            $this->view->featuredTitle = "";
            $this->view->imgthumb = $this->config->application->amazonlink."/uploads/newsimage/".str_replace(" ", "%20", $decoded[0]->imagethumb);
            $this->config->application->amazonlink."/uploads/newsimage/".str_replace(" ", "%20", $decoded[0]->imagethumb);
        }


        $this->view->categorylist = $decoded[0]->categorylist;
        $this->view->date = $decoded[0]->date;
        $this->view->categoryname = $decoded[0]->categoryname;
        $this->view->categoryslugs = $decoded[0]->categoryslugs;
        $this->view->name = $decoded[0]->name;
        $this->view->about = $decoded[0]->about;
        $this->view->image = $decoded[0]->image;

        $this->view->subtitle2 = $decoded[0]->summary;
        $this->view->banner = "blah";
        $this->view->pagecategory = "blog";
        $this->view->pageslugs = $decoded[0]->newsslugs;

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


//        $service_url_feat_video = $this->config->application->ApiURL. '/news/displayvideo';
//
//
//        $curl = curl_init($service_url_feat_video);
//
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//
//        $curl_response = curl_exec($curl);
//
//        if ($curl_response === false)
//        {
//            $info = curl_getinfo($curl);
//            curl_close($curl);
//            die('error occured during curl exec. Additional info: ' . var_export($info));
//        }
//
//        curl_close($curl);
//        $decoded = json_decode($curl_response);
//        $this->view->video = $decoded[0]->video;



        $service_url_news_tags = $this->config->application->ApiURL. '/news/frontend/listtags/' . $newsslugs;


        $curl = curl_init($service_url_news_tags);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->newstags = $decoded;

        $this->angularLoader(array(
            'newscontroller' => 'fe/scripts/controllers/news.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
        ));


        $curl = curl_init($this->config->application->ApiURL.'/news/listtags');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newstagslist = $decoded;

        $curl = curl_init($this->config->application->ApiURL.'/news/frontend/archives');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Addition info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->archivelist = $decoded;
    }

    public function categoryAction($category)
    {

        $this->angularLoader(array(
            'newscontroller' => 'fe/scripts/controllers/news.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
            'filter' => 'fe/scripts/filter.js'
        ));

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listnewsbycategory/' . $category;


        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->listnewsbycategory = $decoded;



        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


        $curl = curl_init($this->config->application->ApiURL.'/news/listtags');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newstagslist = $decoded;

        $curl = curl_init($this->config->application->ApiURL.'/news/frontend/archives');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Addition info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->archivelist = $decoded;

        $this->view->metatitle = ucwords(strtolower(str_replace("-", " ", $category)))." : Blog | Sedona Healing Arts";
        $this->view->subtitle2 = "";
        $this->view->banner = "sedona_blog.jpg";
        $this->view->pagecategory = "blog/category/";
        $this->view->pageslugs = "all";
    }
    public function tagsAction($tags)
    {

        $this->angularLoader(array(
            'newscontroller' => 'fe/scripts/controllers/news.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
            'filter' => 'fe/scripts/filter.js'
        ));

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


        $curl = curl_init($this->config->application->ApiURL.'/news/listtags');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newstagslist = $decoded;

        $curl = curl_init($this->config->application->ApiURL.'/news/frontend/archives');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Addition info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->archivelist = $decoded;


        $this->view->metatitle = ucwords(strtolower(str_replace("-", " ", $tags)))." > Blog ";
        $this->view->subtitle2 = "";
        $this->view->banner = "sedona_blog.jpg";
        $this->view->pagecategory = "blog/tags/";
        $this->view->pageslugs = "all";
    }
    public function allAction()
    {

        $this->angularLoader(array(
            'newscontroller' => 'fe/scripts/controllers/news.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
            'filter' => 'fe/scripts/filter.js'
        ));

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;

        $curl = curl_init($this->config->application->ApiURL.'/news/listtags');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newstagslist = $decoded;

        $curl = curl_init($this->config->application->ApiURL.'/news/frontend/archives');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Addition info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->archivelist = $decoded;

        $book = "Blog";
        $service_url = $this->config->application->ApiURL. '/meta/show/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        // $this->view->title = $decoded[0]->title;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;

        $this->view->title = "Sedona Healing Information Blog | Banya Lim | Sedona Healing Arts";
        // $this->view->metadesc = "Read within our Sedona Healing Arts Blog how to heal your life. Articles include balancing your chakras, what healing energy is and more.";
        
        $this->view->banner = "sedona_blog.jpg";
        $this->view->pagecategory = "blog";
        $this->view->pageslugs = "all";
    }
    public function authorAction($name)
    {
        $this->angularLoader(array(
            'newscontroller' => 'fe/scripts/controllers/news.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
            'filter' => 'fe/scripts/filter.js'
        ));

        $curl = curl_init($this->config->application->ApiURL. '/news/frontend/author/'.$name.'/0/10');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        if($decoded){
            $this->view->title = $decoded[0]->name;
            $this->view->subtitle2 = "";
            $this->view->banner = "sedona_blog.jpg";
            $this->view->pagecategory = "blog/author";
            $this->view->pageslugs = $name;
            $this->view->metatitle = "Blog";
        }else {
            $this->response->redirect($this->config->application->BaseURL.'/page404');
        }
    }


    public function previewAction()
    {
        $this->view->title = $_GET['title'];
        $this->view->author = $_GET['author'];
        $this->view->summary = $_GET['summary'];
        $this->view->body = $_GET['body'];

        $this->view->metatitle = $_GET['metatitle'];
        $this->view->metadesc = $_GET['metadesc'];
        $this->view->metakeyword = $_GET['metakeyword'];

        $this->view->authorname = $_GET['authorname'];
        $this->view->authorimage = $_GET['authorimage'];
        $this->view->authorabout = $_GET['authorabout'];
        $this->view->tagname = $_GET['tagname'];
        $this->view->categoryname = $_GET['categoryname'];

        if($_GET['featuredthumbtype'] == 'video'){
            $this->view->featured = '<iframe width="100%" height="400px" src="https://www.youtube.com/embed/'.$_GET['featuredthumb'].'" frameborder="0" allowfullscreen></iframe>';
            $this->view->featuredTitle = "Featured Video";
        }elseif($_GET['featuredthumbtype'] == 'image'){
            $this->view->featured = '<div class="featured-image" style="background-image: url(\''.$this->config->application->amazonlink .'/uploads/newsimage/'. $_GET['featuredthumb'].'\')"></div>';
            $this->view->featuredTitle = "";
        }


        $this->view->category = $_GET['category'];
        $this->view->date = $_GET['date'];
        $this->view->categoryname = $_GET['categoryname'];

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


//        $service_url_feat_video = $this->config->application->ApiURL. '/news/displayvideo';
//
//
//        $curl = curl_init($service_url_feat_video);
//
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//
//        $curl_response = curl_exec($curl);
//
//        if ($curl_response === false)
//        {
//            $info = curl_getinfo($curl);
//            curl_close($curl);
//            die('error occured during curl exec. Additional info: ' . var_export($info));
//        }
//
//        curl_close($curl);
//        $decoded = json_decode($curl_response);
//        $this->view->video = $decoded[0]->video;


    }

    public function archiveAction($month, $year) {
        $this->angularLoader(array(
            'newscontroller' => 'fe/scripts/controllers/news.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
            'filter' => 'fe/scripts/filter.js'
        ));

         $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


        $curl = curl_init($this->config->application->ApiURL.'/news/listtags');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newstagslist = $decoded;

        $curl = curl_init($this->config->application->ApiURL.'/news/frontend/archives');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Addition info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->archivelist = $decoded;


        $this->view->metatitle = $month." > ".$year . " | Sedona Healing Arts";
        $this->view->subtitle2 = "";
        $this->view->banner = "sedona_blog.jpg";
        $this->view->pagecategory = "blog/archives/";
        $this->view->pageslugs = "all";
    }

}
