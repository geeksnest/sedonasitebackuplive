<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class MaintenanceController extends \Phalcon\Mvc\Controller
{
    public function indexAction()
    {
        $curl = curl_init($this->config->application->ApiURL.'/settings/load');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response === false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->message = $decoded->maintenance->message;

        if($decoded->maintenance->enddate < strtotime(date('Y-m-d')) || $decoded->maintenance->status == 0){
            $this->response->redirect($this->config->application->BaseURL);
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
