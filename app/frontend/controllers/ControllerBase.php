<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    public function onConstruct(){

        $this->view->banner = '';
        $this->view->thumb = '';
        $this->view->iconplay = '';
        $this->view->name = '';
        $this->view->facebookpixels = '';
        
        /*maintenance*/
        $curl = curl_init($this->config->application->ApiURL.'/settings/load');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response === false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $maintenance = $decoded->maintenance;
        if($maintenance->status == 1){
            if($maintenance->enddate < strtotime(date('Y-m-d H:i:s'))){
                $curl = curl_init($this->config->application->ApiURL.'/settings/off/maintenance');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $curl_response = curl_exec($curl);

                if($curl_response === false){
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    die('error occured during curl exec. Additional info: ' . var_export($info));
                }

                curl_close($curl);
            }else {
                $this->response->redirect($this->config->application->BaseURL.'/maintenance');
            }
        }

        $service_url_menu_healing = $this->config->application->ApiURL. '/menu/healing';

        $curl = curl_init($service_url_menu_healing);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $healing = $decoded;
        $this->view->healing = $decoded;



        $service_url_menu_readings = $this->config->application->ApiURL. '/menu/readings';

        $curl = curl_init($service_url_menu_readings);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->readings = $decoded;
        $readings = $decoded;


        $service_url_menu_acupuncture = $this->config->application->ApiURL. '/menu/acupuncture';

        $curl = curl_init($service_url_menu_acupuncture);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->acupuncture = $decoded;
        $acupuncture = $decoded;


        $service_url_menu_retreats = $this->config->application->ApiURL. '/menu/retreats';

        $curl = curl_init($service_url_menu_retreats);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->retreats = $decoded;
        $retreats = $decoded;


        $service_url_menu_workshops = $this->config->application->ApiURL. '/menu/workshops';

        $curl = curl_init($service_url_menu_workshops);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->workshops = $decoded;
        $workshops = $decoded;

        $this->view->imageLink = $this->config->application->amazonlink;


        $sidebar = "<div class='phar no-padding'>    <br/>
                                    <hr class='styled-hr'>
                                    <h3>ALL SERVICES</h3>  <br/>
                                    <span class='size16 font2 side-bar-color'>
                                    <a href='/healing'>HEALING</a><br/>
                                    </span>
                                    <ul>";

        foreach($healing as $h){
            $sidebar .= "<li><a href='/healing/".$h->pageslugs."'>".$h->title."</a></li>";
        }

        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='/readings'>READINGS</a><br/>
                                    </span>
                                    <ul>";
        foreach($readings as $r){
            $sidebar .= "<li><a href='/readings/".$r->pageslugs."'>".$r->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='/acupuncture'>ACUPUNCTURE</a><br/>
                                    </span>
                                    <ul>";
        foreach($acupuncture as $a){
            $sidebar .= "<li><a href='/acupuncture/".$a->pageslugs."'>".$a->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='/retreats'>RETREATS</a><br/>
                                    </span>
                                    <ul>";
        foreach($retreats as $rt){
            $sidebar .= "<li><a href='/retreats/".$rt->pageslugs."'>".$rt->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='/workshops'>WORKSHOPS</a><br/>
                                    </span>
                                    <ul>";
        foreach($workshops as $w){
            $sidebar .= "<li><a href='/workshops/".$w->pageslugs."'>".$w->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <hr class='styled-hr'>
                                </div>";

        $this->view->sidebar = $sidebar;


        $this->view->metatitle = 'Sedona Healing Arts';
        $this->view->metadesc = '';
        $this->view->metakeyword = '';
        $this->view->author = '';


         // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->ApiURL. '/settings/load';

        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->gscript = $decoded->gscript->script;

        $service_url_news = $this->config->application->ApiURL. '/pages/managepage/100/0/null';

        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->metathumbnails = $decoded;

    }

    protected function initialize()
    {

        $this->view->base_url = $this->config->application->BaseURL;
        $this->view->api_url = $this->config->application->ApiURL;

    }

    public function angularLoader($ang){
        $modules = array();
        $scripts = '';
        foreach($ang as $key => $val){
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->otherjvascript = $scripts;
    }
    public function httpPost($url,$params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach($params as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }
    
     public function curl($url){
        $service_url = $this->config->application->ApiURL.'/'.$url;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
    }

}
