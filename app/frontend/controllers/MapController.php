<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;
class MapController extends ControllerBase
{

    public function indexAction()
    {
        echo "Boom Panes!";
    }
    public function startAction()
    {
        $this->angularLoader(array(
            'login' => 'fe/scripts/controllers/login.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'maps' => 'fe/scripts/controllers/maps.js'
        ));
    }
    public function createAction()
    {
        $this->angularLoader(array(
            'login' => 'fe/scripts/controllers/login.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'maps' => 'fe/scripts/controllers/maps.js',
            'mapsFactory' => 'fe/scripts/factory/maps.js',
            'mapsDirective' => 'fe/scripts/directives/maps.js'
        ));
    }

}
