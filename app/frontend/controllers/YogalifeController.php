<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class YogalifeController extends ControllerBase
{
    public function indexAction()
    {

        //list Category

        $service_url_news = $this->config->application->ApiURL. '/news/frontend/listcategory';


        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded;


        //featured news
        $service_url = $this->config->application->ApiURL. '/news/frontend/featurednews';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->featurednews = $decoded;

        //latest news
        $service_url = $this->config->application->ApiURL. '/news/frontend/latest';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->latestnews = $decoded;

        //founders wisdom
        $service_url = $this->config->application->ApiURL. '/news/frontend/founderswisdom';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->founderswisdom = $decoded;
    	
    }

}

