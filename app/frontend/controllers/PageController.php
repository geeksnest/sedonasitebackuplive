<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PageController extends ControllerBase
{
    public function indexAction($pageslugs)
    {
        if($pageslugs) {
            $this->response->redirect($this->config->application->BaseURL.'/page404');
        }
        // $pageidglobal = 0;
        // var_dump($pageslugs);
        // die();
        // //view page content
        // $service_url = $this->config->application->ApiURL.'/page/getpage/' . $pageslugs;
        // $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);
        // if ($curl_response === false) {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additioanl info: ' . var_export($info));
        // }
        // curl_close($curl);
        // $decoded = json_decode($curl_response);
        // $this->view->pageid = $decoded->pageid;
        // $this->view->title = $decoded->title;
        // $this->view->subtitle1 = $decoded->subtitle1;
        // $this->view->metadesc = $decoded->metadesc;
        // $this->view->buttontitle = $decoded->buttontitle;
        // $this->view->pageslugs = $decoded->pageslugs;
        // $this->view->body = $decoded->body;
        // $this->view->peoplesaying = $decoded->peoplesaying;
        // $this->view->serviceprice = $decoded->serviceprice;
        // $this->view->banner = $decoded->banner;
        // $this->view->pagelayout = $decoded->pagelayout;

        // $pageidglobal = $decoded->pageid;

        //  // GOOGLE ANALYTICS
        // $service_url_news = $this->config->application->ApiURL. '/settings/load/googlescript';

        // $curl = curl_init($service_url_news);

        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);

        // if ($curl_response === false)
        // {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additional info: ' . var_export($info));
        // }

        // curl_close($curl);
        // $decoded = json_decode($curl_response);

        // $this->view->gscript = $decoded[0]->script;
    }
    public function viewAction($pageslugs)
    {

        $pageidglobal = 0;


        //view page content
        $service_url = $this->config->application->ApiURL.'/page/getpage/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->pageid = $decoded->pageid;
        $this->view->title = $decoded->title;
        $this->view->subtitle1 = $decoded->subtitle1;
        $this->view->metadesc = $decoded->imagethumbsubtitle;
        $this->view->buttontitle = $decoded->buttontitle;
        $this->view->pageslugs = $decoded->pageslugs;
        $this->view->body = $decoded->body;
        $this->view->peoplesaying = $decoded->peoplesaying;
        $this->view->serviceprice = $decoded->serviceprice;
        $this->view->banner = $decoded->banner;
        $this->view->pagelayout = $decoded->pagelayout;
        $this->view->imgthumb =  $this->config->application->amazonlink."/uploads/pageimage/".$decoded->banner;
        $this->view->metatitle = $decoded->metatitle;
        $this->view->metadesc = $decoded->metadesc;
        $this->view->metakeyword = $decoded->metatags;
        
        if($decoded->status == 0){
            $this->response->redirect($this->config->application->BaseURL.'/page404');
        }

        $pageidglobal = $decoded->pageid;

        $service_url = $this->config->application->ApiURL.'/page/getpagecategory/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->pagecategory = $decoded->categorytitle;
        //list testemonils
        $service_url_testi = $this->config->application->ApiURL. '/page/gettestimonial/' . $pageslugs;


        $curl = curl_init($service_url_testi);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->testimonials = $decoded;

    }


     public function healingAction()
    {
        $book = "Healing";
        $service_url = $this->config->application->ApiURL. '/meta/showcat/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;

        $this->view->title = "Sedona Energy Healing | Healing Touch - Sedona Healing Arts";
        $this->view->banner = "healing.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "healing";
    }

     public function chakrabalancingandcrystalhealingAction()
    {

    }

     public function reflexologyAction()
    {

    }

     public function relaxationAction()
    {

    }

     public function hotstonemassageAction()
    {

    }

     public function deeptissuemassageAction()
    {

    }

     public function readingsAction()
    {
        $book = "Readings";
        $service_url = $this->config->application->ApiURL. '/meta/showcat/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;
        $this->view->banner = "readings.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "readings";
    }

     public function spiritualguidanceAction()
    {

    }

     public function intuitivereadingAction()
    {

    }

     public function pastlifereadingAction()
    {

    }

     public function couplesreadingAction()
    {

    }

     public function acupunctureAction()
    {
        $book = "Acupuncture";
        $service_url = $this->config->application->ApiURL. '/meta/showcat/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;
        $this->view->banner = "acupuncture.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "acupuncture";
    }

     public function spiritualacupunctureAction()
    {

    }

     public function elementsacupunctureAction()
    {

    }

     public function retreatsAction()
    {
        $book = "Retreats";
        $service_url = $this->config->application->ApiURL. '/meta/showcat/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;
        $this->view->banner = "sedona_baner_retreats.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "retreats";
    }

     public function healyourbodysoul2daysAction()
    {

    }

     public function findyourpurpose3or4daysAction()
    {

    }

     public function manifestyourdream5or6daysAction()
    {

    }

     public function workshopsAction()
    {
        $book = "Workshops";
        $service_url = $this->config->application->ApiURL. '/meta/showcat/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;
        $this->view->banner = "workshops_banner.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "workshops";
    }

     public function guidedmeditationAction()
    {

    }

     public function energyopeningtrainingAction()
    {

    }

     public function thirdeyeopeningleveloneAction()
    {

    }

     public function vortexhikemeditationAction()
    {

    }

     public function crystalpalacevisitguidedmeditationAction()
    {

    }

     public function bodymindspiritintegrationAction()
    {

    }

     public function bookingAction()
    {
        $book = "Booking";
        $service_url = $this->config->application->ApiURL. '/meta/show/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;

        $this->angularLoader(array(
            'booking' => 'fe/scripts/controllers/booking.js',
            'birthdayfactory' => 'fe/scripts/factory/birthday.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'validation' => 'fe/scripts/directives/validations.js',
            'passstrength' => 'vendors/pass-strength/src/strength.min.js'
        ));
        unset($this->view->imgthumb);
        $this->view->pagecategory = "";
        $this->view->pageslugs = "booking";
        $this->view->facebookpixels= "    <script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
    _fbq.push(['addPixelId', '1617141355217381']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>";
    }

    public function aboutsedonahealingartsAction()
    {
        $book = "AboutUs";
        $service_url = $this->config->application->ApiURL. '/meta/show/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;

        $this->view->banner = "banner_about.png";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "aboutsedonahealingarts";
    }

    public function contactusAction()
    {
        $book = "ContactUs";
        $service_url = $this->config->application->ApiURL. '/meta/show/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;

        $this->angularLoader(array(
            'contactus' => 'fe/scripts/controllers/contactus.js'
        ));

        unset($this->view->imgthumb);
        $this->view->pagecategory = "";
        $this->view->pageslugs = "contactus";

        $this->view->facebookpixels= "    <script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
    _fbq.push(['addPixelId', '1617141355217381']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>";
    }

    public function feedAction()
    {
      $service_url = $this->config->application->ApiURL.'/news/frontend/listnews/0/10';
      $curl = curl_init($service_url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $curl_response = curl_exec($curl);
      if ($curl_response === false) {
          $info = curl_getinfo($curl);
          curl_close($curl);
          die('error occured during curl exec. Additioanl info: ' . var_export($info));
      }
      curl_close($curl);
      $decoded = json_decode($curl_response);

      $this->view->items = $decoded;

      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function page404Action(){
      $this->view->metatitle = 'Page not found';
      $this->view->title = "Page not found";
      $this->view->metadesc = "";
      $this->view->banner = "404.jpg";
      $this->view->pagecategory = "";
      $this->view->pageslugs = "";
    }
}
