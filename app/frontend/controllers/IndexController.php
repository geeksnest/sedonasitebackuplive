<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class IndexController extends ControllerBase
{
    public function indexAction()
    {

    	//latest news
        $service_url = $this->config->application->ApiURL. '/news/frontend/latest';


        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->latestnews = $decoded;
        
        $curl = curl_init($this->config->application->ApiURL.'/testimonials/list/100/0/null');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if($curl_response == false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->testimonials = $decoded;
        
        $book = "Homepage";
        $service_url = $this->config->application->ApiURL. '/meta/show/' . $book;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->metatitle = $decoded[0]->metatitle;
        $this->view->metadesc = $decoded[0]->metadesc;
        $this->view->metakeyword = $decoded[0]->metakeyword;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

