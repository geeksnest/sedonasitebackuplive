<!-- ===== SIDE BAR 2 ===== -->
        <div class="phar no-padding">    <br/>     
          <hr class="styled-hr">
          <h3>ALL SERVICES</h3>  <br/>      
          <span class="size16 font2 side-bar-color">
            <a href="">HEALING SERVICES</a><br/>          
          </span>   
          <ul>
            <li><a href="">Intuitive Reading</a></li>
            <li><a href="">Past Life Reading</a></li>
            <li><a href="">Couple's Relationship Reading</a></li>
            <li><a href="">Chakra Balancing and Crystal Healing</a></li>
            <li><a href="">Spiritual Acupuncture</a></li>
          </ul>    
          <span class="size16 font2 side-bar-color">
            <a href="">RETREATS & WORKSHOPS</a><br/>          
          </span>   
          <ul>
            <li><a href="">Heal Your Body & Soul</a></li>
            <li><a href="">Find Your Purpose</a></li>
            <li><a href="">Manifest Your Dream</a></li>
            <li><a href="">Body, Mind & Spirit Integration</a></li>
            <li><a href="">Crystal Palace Workshop</a></li>
          </ul>    
          <span class="size16 font2 side-bar-color">
            <a href="">MASSAGE</a><br/>          
          </span>   
          <ul>
            <li><a href="">Relaxation Massage</a></li>
            <li><a href="">Meridian Massage</a></li>
            <li><a href="">Deep Tissue Massage</a></li>
            <li><a href="">Hot Stone Massage</a></li>
            <li><a href="">Reflexology</a></li>
          </ul>           
          <hr class="styled-hr">
        </div>
<!-- ===== END SIDE BAR 2 ===== -->