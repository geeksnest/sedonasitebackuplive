<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  
  <style>
    h2 {
      text-align: center;
      margin-top: 5%;
    }
    span {
      font-size: 16px;
      display: block;
      margin: auto;
      text-align: center;
    }
    .logo {
      margin-top: 20%;
    }
  </style>
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="{{url.getBaseUri()}}public/img/favicon/favicon.png">-->
</head>

<body>
  <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController">
    <div class="m-b-lg logo">
      <div class="wrapper text-center">
        <img src='/img/frontend/sedona_logo.png' style="width:100%;height:auto;">
      </div>
    </div>
  </div>

  <span>Sedona Healing Arts website is under maintenance.</span>
  <h2><span class="fa fa-quote-left"></span> {{ message }}</h2>
 
</body>
</html>