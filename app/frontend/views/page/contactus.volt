<!-- Banner starts -->
<!--  <div class="banner-container healing-bg">

    </div> -->

<!-- Banner ends -->
<style type="text/css">
  [ng\:cloak], [ng-cloak], .ng-cloak {
  display: none !important;
}
</style>

<div class="container" ng-controller="ContactusCtrl" id="">
  <div class="row">
    <br/>
    <br/>
    <br/>
    <div class="col-sm-12">
      <h1>Contact Us</h1>
    </div>
    <div class="col-sm-6">
      <div class="form">
        <!-- Contact form -->
        <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
          <!-- Name -->
          <div class="control-group margin-bot8">
            <div class="row">
              <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                <br>We will review your inquiry and get back to you shortly.</label>
            </div>
          </div>
          <div class="control-group margin-bot8">
            <div class="row">
              <label class="control-label size14 col-sm-12" for="name">Subject
                <span class="red">*</span>
              </label>
              <div class="col-sm-12">
                <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
              </div>
            </div>
          </div>
          <div class="control-group margin-bot8">
            <div class="row">
              <label class="control-label size14 col-sm-12" for="name">Name
                <span class="red">*</span>
              </label>
              <div class="col-sm-6">
                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
              </div>
              <div class="col-sm-6">
                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
              </div>
            </div>
          </div>
          <!-- Email -->
          <div class="control-group margin-bot8">
            <div class="row">
              <label class="control-label size14 col-sm-12" for="email">Email
                <span class="red">*</span>
              </label>
              <div class="col-sm-12">
                <span class="red hidden booking-email">Invalid Email Address</span>
                <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
              </div>
            </div>
          </div>
          <!-- Comment -->
          <div class="control-group margin-bot8">
            <div class="row">
              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                <span class="red">*</span>
              </label>
              <div class="col-sm-12">
                <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
              </div>
            </div>
          </div>
          <!-- Google recaptcha -->
          <div class="control-group margin-bot8">
            <div class="row">
              <div class="col-sm-12"> 
                <div vc-recaptcha key="'6Ld1twsTAAAAALIwIlJMw2FCP_X8gYpwbh-kLroT'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
              </div>
            </div>
          </div>
          <!-- Buttons -->
          <div class="form-actions margin-bot8">
            <div class="row">
              <div class="col-sm-12">
                <div class="col-sm-3">
                   <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid || res==false">Submit</button>
                </div>
                <div class="col-sm-3">
                   <div class="loadercontainer" ng-cloak ng-show="imageloader">
                    <div class="btn-group">
                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                      </div>
                      <div class="">Please wait...</div>
                    </div>
                  </div>
              </div>
              </div>
            </div>
          </div>
          <!-- Buttons -->
        </form>
      </div>
      <div class="clearfix"></div>
    </div>

    <div class="col-sm-6 storeinfo">
      <div class="storeinfo-details">
        <h2 class="orange">Store Information</h2>
        <div class="title3">
          <span class="titleb">Phone:</span>
          <span class="thin-font3">(928) 282-3875</span>
          <br/>
          <span class="titleb">Email:</span>
          <span class="thin-font3">contact@sedonahealingarts.com</span>
          <br/>
          <span class="titleb">Hours:</span>
          <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
          <br/>
          <br/>
        </div>
        <div class="title3">
          <span class="titleb text-top">Address:</span>
          <br/>
          <span class="thin-font3">201 State Route 179</span>
          <br/>
          <span class="thin-font3">Sedona, AZ 86336</span>
        </div>

        <div class="wsite-map">
          <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
        </div>






        <div class="clearfix"></div>
      </div>
    </div>

  </div>
</div>

<!-- Below Banner ends -->
