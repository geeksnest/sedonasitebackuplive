
    <!-- Banner starts -->
    <div class="banner-container intuitive-bg">

        <div class="black-box">
            <span class="banner-title">Intuitive Reading</span>
            <br/>
            <span class="banner-sub-title1">Clarity for Any Situation</span> <br/> <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
        </div>

    </div>

    <!-- Banner ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-8 no-margin content-text">
                <div class="phar no-padding">
                    <span class="size20"><h2>INTUITIVE READING</h2></span>
                    <p class="size18">
                        The Intuitive Readings offered at Sedona Healing Arts are both comprehensive and unparalleled amongst their kind.  
                        Key information is read from the highest and most complete level of the soul and distilled into everyday language 
                        so that you may understand (from a higher perspective) why you experience the struggles that you face.  You may 
                        discuss any topic during your session; past or current issues, relationships, work and career, life purpose and 
                        spiritual growth, physical health, mental health, emotional health, finances, etc. You may also ask for insight 
                        into any person who may be a part of your life; past, present, or future.  Intuitive readings are a powerful tool 
                        to bring clarity where there was once confusion, certainty where there was once doubt, and triumph where there were once obstacles. 
                    </p>
                </div>

                <div class="phar no-padding">
                    <div class="size20"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size18 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>
            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>INTUITIVE READING</h2>
                    <h3 class="grayfont">Price: $100 (30 min) / $200 (60 min)</h3>
                    <div class="button">
                    <a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
                    </div><br/>
                    <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->