
    <!-- Banner starts -->
    <div class="banner-container retreats-bg retreats-bg-heal-your-body-soul">
        <div class="black-box">
            <span class="banner-title">Heal your body & soul</span>
            <br/>
            <span class="banner-sub-title1">- 2 days -</span>
            <br/>
            <span class="banner-sub-title1">Customized Healing Experience</span> <br/> <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a></div>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="col-sm-8 no-margin content-text">

                <div class="phar no-padding">
                    <span class="size20"><h2>HEAL YOUR BODY & SOUL</h2></span>
                      <p class="size18">
                        If you need to get a fresh perspective on a situation that is bothering you, release negative feelings and thoughts, 
                        recover from a tragic experience, or if you are just looking to take a break from the hustle and bustle of everyday 
                        life and make a fresh start then the Heal Your Body & Soul Retreat is perfect for you.  This weekend retreat will help 
                        you clear the overwhelm and negative emotions that impact your relationships, confidence, self-trust, and decision making - 
                        allowing you the sacred time and space to reflect on what’s really happening inside of you, providing the clarity and 
                        tools to change the way that you approach the stressful situations in your everyday environment - thus revitalizing and 
                        healing the life of your body, mind, and soul.
                      </p>

                    <p class="size20 title-like-header">The Retreat Includes</p>
                    <div class="padding-left20">
                        <h3>Intuitive Reading and Guidance</h3>
                        <p class="">
                            Key information is read from the highest and most complete level of the soul and distilled into everyday language so 
                            that you may understand (from a higher perspective) why you experience the struggles that you face.  You may discuss 
                            any topic during your session; past or current issues, relationships, work and career, life purpose and spiritual growth, 
                            physical health, mental health, emotional health, finances, etc. You may also ask for insight into any person who may be 
                            a part of your life; past, present, or future.  Intuitive readings are a powerful tool to bring clarity where there was 
                            once confusion, certainty where there was once doubt, and triumph where there were once obstacles.
                        </p>
                        <h3>Customized Energy Training Techniques</h3>
                        <p class="">
                            A customized Mind/Body Energy Training program will be developed based upon your specific physical, mental, emotional, 
                            and spiritual needs.  You will receive training on meridian opening exercises and self-healing energy techniques that 
                            will help you reconnect with the essence of your soul while in Sedona, as well as help you maintain the benefits of 
                            the retreat long after you have returned home.
                        </p>
                    </div>
                </div>

                <div class="phar no-padding">
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size16 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>HEAL YOUR BODY & SOUL</h2>
                    <h1>2 Day Retreat</h1>
                    <h3 class="grayfont">Price: Single $600 / Couple $995</h3>
                                <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
                                <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->