
    <!-- Banner starts -->
    <div class="banner-container spiritual-acupuncture-bg">

        <div class="black-box">
            <span class="banner-title">Spiritual Acupuncture</span>
            <br/>
            <span class="banner-sub-title1">Heal Your Root Issues</span> <br/> <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
        </div>

    </div>

    <!-- Banner ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-8 no-margin content-text">
                <div class="phar no-padding">
                    <span class="size20"><h2>SPIRITUAL ACUPUNCTURE</h2></span>
                    <p class="size18">
                        The Spiritual Acupuncture Session is a unique service that incorporates an intuitive reading into the acupuncture session 
                        providing insight into the reason your body is experiencing stress, tension, or illness based upon the flow of energy 
                        through the meridian system.  Your body literally carries a map of all of the experiences you’ve had in life and most 
                        physical ailments can be traced back to spiritual obstacles or trauma endured in your history.  By understanding the 
                        experiences that caused the blocked energy in the various acupuncture points, you’ll have the opportunity to release the 
                        foundational emotional and spiritual burdens that were the root cause of the physical manifestations of tension and illness.  
                        The combination of the two therapies together is a powerful tool in the process of reinstating the flow of life force energy 
                        in the physical body; leaving you feeling healthier, happier, and with a refreshed sense of well-being.
                    </p>
                </div>

                <div class="phar no-padding">
                    <div class="size20"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size18 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>
            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>SPIRITUAL ACUPUNCTURE</h2>
                    <h3 class="grayfont">Price: $200 (60 min)</h3>
                    <div class="button">
                    <a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
                    </div><br/>
                    <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->