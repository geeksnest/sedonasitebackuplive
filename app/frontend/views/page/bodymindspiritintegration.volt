
    <!-- Banner starts -->
    <div class="banner-container retreats-bg workshops-crystal-palace">
        <div class="black-box">
            <span class="banner-title fontsize50">Body, Mind & Spirit Integration Workshop</span>
            <br/>
            <span class="banner-sub-title1">Experience Universal Consiousness</span>
            <br/><br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">SCHEDULE YOUR WORKSHOP</a></div>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="col-sm-8 no-margin content-text">

                <div class="phar no-padding">
                    <span class="size20"><h2>BODY, MIND & SPIRIT INTEGRATION
                            (DAHNGUN BUDDHA GUIDED MEDITATION & TRAINING)</h2></span>
                      <p class="size18">
                          The Body, Mind, & Spirit Integration Workshop is based on a sacred and precious painting called, DahnGuhn Buddha. The painting depicts different levels of human consciousness, including the ultimate level of Nirvana.  This unique meditation training utilizes the immense energy in this sacred painting to understand one’s consciousness to reach the highest spiritual level. Let your spirit experience the inspiration of ultimate freedom, which is the evolution and completion of the human soul.
                      </p>
                    <span class="size20"><h2>Through the DahnGuhn Buddha Meditation You will Experience: </h2></span>
                    <p>
                    <ul style="">
                        <li><span style="background-color: initial;">Spiritual purification of the emotions, thoughts, and negative memories that keep you from your highest potential.</span></li>
                        <li><span style="background-color: initial;">Development of the acute energy senses that will enable you to feel and expand the energy field that encapsulates your body.</span></li>
                        <li><span style="background-color: initial;">Meet with the highest expression of your divinity, which has always existed within you. </span></li>
                    </ul>
                    </p>
                </div>

                <div class="phar no-padding">
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size16 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>BODY, MIND & SPIRIT INTEGRATION WORKSHOP</h2>
                    <h3 class="grayfont">Cost: $1000 (180 min)</h3>
                                <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
                                <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->