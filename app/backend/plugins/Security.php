<?php
namespace Modules\Backend\Plugins;

use Phalcon\Events\Event,
	Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Acl;
use Phalcon\Acl\Adapter\Memory as Mem;
/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Security extends Plugin
{
	public function __construct($dependencyInjector)
	{
		$this->_dependencyInjector = $dependencyInjector;
	}
	public function getAcl()
	{
		//if (!isset($this->persistent->acl)) {

			$acl = new Mem();

			$acl->setDefaultAction(\Phalcon\Acl::DENY);

			//Register roles
			$roles = array(
				'superagents' => new \Phalcon\Acl\Role('SuperAgent'),
				'guests' => new \Phalcon\Acl\Role('Guests')
			);

			foreach ($roles as $role) {
				$acl->addRole($role);
			}

			//Private area resources
			$privateResources = array(
				'admin' => array('dashboard','index','boom','logout'),
				'users' => array('create', 'createSave', 'userlist','edituser'),
				'pages' => array('createpage','managepage','editpage','managepagecategory'),
				'testimonials' => array('index'),
				'booking' => array('index'),
				'metadata' => array('index'),
				'settings' => array('index'),
				'news' => array('createnews','managenews','editnews','editnewscenter','category','tags','createauthor','manageauthor','editauthor'),
				'contactus' => array('index'),
				'profile' => array('index')
			);
			foreach ($privateResources as $resource => $actions) {
				$acl->addResource(new \Phalcon\Acl\Resource($resource), $actions);
			}

			//Public area resources
			$publicResources = array(
				'index' => array('index', 'logout'),
				'forgotpassword' => array('index', 'changepassword')
			);
			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new \Phalcon\Acl\Resource($resource), $actions);
			}

			//Grant access to public areas to both users and guests
			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					$acl->allow($role->getName(), $resource, $actions);
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('SuperAgent', $resource, $action);
				}
			}

			//The acl is stored in session, APC would be useful here too
			$this->persistent->acl = $acl;
		//}

		return $this->persistent->acl;
	}

	/**
	 * This action is executed before execute any action in the application
	 */
	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
	{

		$auth = $this->session->get('auth');
		if (!$auth){
			$role = 'Guests';
		} else {
			$role = 'SuperAgent';
		}
		$controller = $dispatcher->getControllerName();
		$action = $dispatcher->getActionName();

		$acl = $this->getAcl();

		$allowed = $acl->isAllowed($role, $controller, $action);

		if ($allowed != Acl::ALLOW) {
			$dispatcher->forward(
				array(
					'controller' => 'index',
					'action' => 'index'
				)
			);
			return false;
		}
	}
}
