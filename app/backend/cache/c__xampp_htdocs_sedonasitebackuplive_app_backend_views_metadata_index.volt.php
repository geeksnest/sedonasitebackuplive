<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="updateMeta.html">
    <div ng-include="'/be/tpl/updateMeta.html'"></div>
</script>

<script type="text/ng-template" id="replyBooking.html">
    <div ng-include="'/be/tpl/replyBooking.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Meta Data <!-- <button class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="addmeta()"><i class="fa fa-plus"></i>Add Meta Data </button> -->
    </h1>

    <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formbooking">
    <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

            <div class="row">

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            List
                        </div>

                        <div class="panel-body">

                            <div class="row wrapper">
                                <div class="col-sm-4 m-b-xs" ng-show="keyword || searchdate">
                                    <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong>  <span ng-show="keyword">"{[{ keyword }]}"</span></strong>  <span ng-show="keyword && searchdate">OR</span> <strong><span ng-show="searchdate">"{[{ searchdate }]}"</span></strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                                </div>
                                <div class="col-sm-3 m-b-xs pull-right">
                                    <div class="input-group">
                                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                                        <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext, searchdateelem)">Go!</button>
                                        </span>
                                    </div>
                                </div>
                               <!--  <div class="col-sm-3 m-b-xs pull-right">
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <input id="date" name="date" class="input-sm form-control" datepicker-popup="yyyy-MM-dd" ng-model="searchdateelem" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                                        <button type="button" class="btn btn-sm btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                      </span>
                                    </div>
                                </div> -->
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                    <thead>
                                    <tr>
                                        <th style="width:15%">Module</th>
                                        <th style="width:15%">Meta Title</th>
                                        <th style="width:15%">Meta Description</th>
                                        <th style="width:20%">Meta Keywords</th>
                                        <th style="width:10%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody ng-show="loading">
                                        <tr >
                                            <td colspan="7">Loading Data</td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-hide="loading">
                                    <tr ng-show="bigTotalItems==0"> <td colspan="7"> No records found! </td></tr>
                                    <tr ng-repeat="meta in data.data">
                                        <td>
                                            <span class="font-bold" ng-bind="meta.module"></span>
                                        </td>
                                        <td>
                                            <span ng-bind="meta.metatitle"></span>
                                        </td>
                                        <td>
                                            <span ng-bind="meta.metadesc"></span>
                                        </td>
                                        <td>
                                            <span ng-bind="meta.metakeyword"></span>
                                        </td>
                                       
                                        <td>
                                    <a href class="btn btn-info btn-default btn-xs" ng-click="edit(meta)">
                                                <i class="fa fa-edit"></i>
                                                Update
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>



                        </div>


                    </div>
                </div>

            </div>

            <div class="row" ng-hide="bigTotalItems==0 || loading">
                <div class="panel-body">
                    <footer class="panel-footer text-center bg-light lter">
                        <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                        <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                    </footer>
                </div>
            </div>

        </div>
    </fieldset>
</form>