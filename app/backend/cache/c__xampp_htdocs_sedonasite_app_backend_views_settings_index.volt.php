<?php echo $this->getContent(); ?>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Settings</h1>
    <a id="top"></a>
</div>

<div class="row wrapper-md">
    <div class="col-sm-6">
        <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="maintenance">
            <fieldset ng-disabled="isSaving">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading font-bold">
                                Maintenance
                            </div>
                            <div class="panel-body">
                                <div class="wrapper-md" style="padding-top : 0px;">
                                    <div class="line line-dashed b-b line-lg pull-in"></div>            
                                    <span class="help-block m-b-none" ng-show="status==false">In order to to turn on maintenance mode you must fill all the fields.</span>   
                                    <span class="help-block m-b-none" ng-show="status">Maintenance mode is turned on.</span>           
                                    <div class="line line-dashed b-b line-lg pull-in"></div>
                                    <div class="form-group hiddenoverflow" ng-show="status==0">
                                        <label class="col-sm-3 control-label">Message</label>
                                        <div class="col-sm-9">
                                          <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" required="required" ng-model="mainte.message">
                                        </div>
                                    </div>
                                    <div class="form-group" ng-show="status">
                                        <label class="col-sm-12 control-label">{[{ mainte.message }]}</label>
                                    </div>
                                    <div class="line line-dashed b-b line-lg pull-in"></div>
                                    <div class="form-group" ng-show="status==false">
                                        <label class="col-sm-3 control-label">Time</label>
                                        <div class="col-sm-4">
                                            <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                            <input ui-jq="TouchSpin" type="text" value="" class="form-control" data-min="0" data-max="100" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;" required="required" ng-model="mainte.hour" onkeypress='return isNumberKey(event)'> 
                                            <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                            </div>
                                        </div><br/><br/>
                                    </div>
                                    <div class="form-group" ng-show="status">
                                        <label class="col-sm-12 control-label">{[{ mainte.hour }]} hour/s maintenance.</label>
                                    </div>
                                    <div class="line line-dashed b-b line-lg pull-in"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">Off</spam> 
                                        <div class="m-b-sm col-sm-2">
                                          <label class="i-switch i-switch-lg m-t-xs m-r">
                                            <input type="checkbox" ng-model="mainte.status" id="maintestatus" ng-required="required">
                                            <i></i>
                                          </label>
                                        </div>
                                        <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">On</spam> 
                                    </div> 
                                    <div class="line line-dashed b-b line-lg pull-in"></div>
                                    <div class="col-sm-12" ng-show="status==false">
                                        <button type="submit" class="btn btn-sm  btn-success" ng-click="savemainte(mainte)" ng-disabled="maintenance.$invalid" style="float:right;">Save change</button>
                                    </div> 
                                    <div class="col-sm-12" ng-show="status">
                                        <button type="submit" class="btn btn-sm  btn-success" ng-click="offmainte()" ng-disabled="mainte.status" style="float:right;">Save change</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="col-sm-6">
        <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savescript(google)" name="googlescript">
            <fieldset ng-disabled="isSaving">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading font-bold">
                                Google Analytics
                            </div>

                            <div class="panel-body">

                                <div class="wrapper-md">
                                    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    					        <div class="form-group">
    					        	<label class=" control-label">Script</label>
    					        	<input type="text" id="gscript" name="gscript" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern ng-pristine ng-untouched" ng-model="google.script" required="required">
    					        	<br>
    					        </div>
    					        <footer class="panel-footer text-right bg-light lter">
    					         	<button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="googlescript.$invalid">Save</button>
    					       </footer>
    				            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>