<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="google-site-verification" content="olwczc3nc3-vWAy4G2_0-L87S-Sjw_HvXrYI26uRjKc" />

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('be/css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/animate.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font-awesome.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/simple-line-icons.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/app.css'); ?>
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">-->
</head>

<body>
  <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController">
  <a href class="navbar-brand block m-t">Sedona Healing Arts</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <img src='/img/frontend/sedona_logo.png' style="width:100%;height:auto;">
    </div>
    <form name="form" class="form-validation" method="post" action="/sedonaadmin">
      <div class="text-danger wrapper text-center" ng-show="authError">
          <?php echo $this->getContent(); ?>
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          <?php echo $this->tag->textField(array('username', 'size' => '30', 'class' => 'form-control no-border', 'id' => 'inputUsername', 'placeholder' => 'Username', 'ng-model' => 'user.password', 'required' => 'required')); ?>
        </div>
        <div class="list-group-item">
          <?php echo $this->tag->passwordField(array('password', 'size' => '30', 'class' => 'form-control no-border', 'id' => 'inputPassword', 'placeholder' => 'Password', 'ng-model' => 'user.password', 'required' => 'required')); ?>
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
      <div class="text-center m-t m-b"><a href="/sedonaadmin/forgotpassword">Forgot password?</a></div>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Only the Administrator can access beyond this point.</small></p>
    </form>
  </div>
  <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div>
</div>
<!-- JS -->
<?php echo $this->tag->javascriptInclude('be/js/jquery/jquery.min.js'); ?>
<!-- angular -->
<?php echo $this->tag->javascriptInclude('be/js/angular/angular.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-ui-router.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-validate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-bootstrap-tpls.min.js'); ?>


<!-- APP -->
 
</body>
</html>