{{ content() }}


<script type="text/ng-template" id="pagecategoryAdd.html">
  <div ng-include="'/be/tpl/pagecategoryAdd.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-9">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
            <div class="panel-body">

              Menu Page Category              
              <select class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required" ng-model="page.category" >

                <option value="{[{ data.id }]}" ng-repeat="data in catlist"> {[{ data.title }]} </option>
              </select>
              <br>
              <div class="form-inline">
                <a href="" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus"></i>Add category</a>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <input type="hidden" id="title" name="title" ng-model="page.pageid" >
              Title
              <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
              <br>
              <b>Page Slugs: </b>
              <input type="hidden" ng-model="page.pageslugs"><span ng-bind="page.pageslugs"></span>

              <div class="line line-dashed b-b line-lg"></div>
              Sub-title 1
              <input type="text" id="subtitle1" name="subtitle1" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.subtitle1">
              Sub-title 2
              <input type="text" id="subtitle2" name="subtitle2" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.subtitle2">

              <div class="line line-dashed b-b line-lg"></div>

              Button Title
              <input type="text" id="buttontitle" name="buttontitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.buttontitle" required="required" >

              <div class="line line-dashed b-b line-lg"></div>

              Body Content
              <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
              
            </div>
          </div>
        </div>

        <div class="col-sm-3">

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
            Page Banner
            </div>
            <div class="panel-body">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.banner }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="banner" name="banner" class="form-control" ng-model="page.banner"  placeholder="Paste Banner link here..." required="required" onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.banner, 'banner')">
            </div>
          </div>
          
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
            	Meta
            </div>
            <div class="panel-body">
               Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" required="required" placeholder="Meta Title">
              
              <div class="line line-dashed b-b line-lg"></div>
              	Description
              <input input="text"  id="metadesc" name="metadesc"  class="form-control" ng-model="page.metadesc" required="required" placeholder="Meta Description">
              
              <div class="line line-dashed b-b line-lg"></div>
              	Keyword
              <input type="text" id="metatags" name="metatags" class="form-control" ng-model="page.metatags" required="required" placeholder="Meta Keyword">
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Thumbnail
            </div>
            <div class="panel-body">
              <img ng-show="page.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.imagethumb }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="imagethumb" name="imagethumb" class="form-control" ng-model="page.imagethumb"  placeholder="Paste Image Thumbnail link here..." required="required" onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.imagethumb, 'thumb')">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.imagethumbsubtitle"  placeholder="Paste Image Thumbnail Subtitle" required="required" >
              <div class="line line-dashed b-b line-lg"></div>
              <textarea type="text" id="thumbdesc" name="thumbdesc" class="form-control" ng-model="page.thumbdesc"  placeholder="Description" required="required" > </textarea>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body">
                Service Price
                <input type="text" id="serviceprice" name="serviceprice" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.serviceprice" required="required" >  
              </div> 
          </div>

          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar Switch
            </div>

            <div class="panel-body">

              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="a" ng-model="page.layout" value="1" ng-click="radio('1')" required="required" checked="true">
                  <i></i>
                  Full Page Layout
                </label>
              </div>

              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="a" ng-model="page.layout" value="2" ng-click="radio('2')" required="required">
                  <i></i>
                  Page with Sidebar Layout
                </label>
              </div>              
            </div>
          </div> -->
          
        </div> 

        <!-- <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body">
                Service Price
                <input type="text" id="serviceprice" name="serviceprice" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.serviceprice" required="required" >  
              </div>  

          </div>
        </div>  --> 


      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="managepage" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>


      <div  class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Image Gallery
              </div>




                <div class="panel-body">
                  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
                  <div class="loader" ng-show="imageloader">
                    <div class="loadercontainer">
                      
                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                      Uploading your images please wait...

                    </div>
                    
                  </div>

                  <div ng-show="imagecontent">

                    <div class="col-sml-12">
                      <div class="dragdropcenter">
                        <div ngf-drop ngf-select ng-model="files" class="drop-box" 
                        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*,application/pdf">Drop images here or click to upload</div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="col-sm-3" ng-repeat="data in imagelist">
                     <a href="" ng-click="deletepagesimg(data.filename)" class="closebutton">&times;</a>
                      <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
                      <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}');">
                      </div>
                    </div>

                  </div>


                </div>

          </div>
        </div>

      </div>

  </div>
</fieldset>
</form>