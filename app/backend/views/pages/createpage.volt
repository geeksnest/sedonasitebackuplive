{{ content() }}


<script type="text/ng-template" id="pagecategoryAdd.html">
  <div ng-include="'/be/tpl/pagecategoryAdd.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Page</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>


              <div class="panel-body">

                Menu Page Category
                <select class="form-control" required="required" ng-model="page.category" >
                  <option value="{[{ data.id }]}" ng-repeat="data in catlist"> {[{ data.title }]} </option>
                </select>
                <br>

                <div class="form-inline">
                    <a href="" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus"></i>Add category</a>

                </div>

                <div class="line line-dashed b-b line-lg"></div>

                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
                <br>
                <b>Page Slugs: </b>
                <input type="hidden" ng-model="page.slugs"><span ng-bind="page.slugs"></span>

                <div class="line line-dashed b-b line-lg"></div>

                Sub-title 1
                <input type="text" id="subtitle" name="subtitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.subtitle1">
                Sub-title 2
                <input type="text" id="subtitle" name="subtitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.subtitle2">

                <div class="line line-dashed b-b line-lg"></div>

                Button Title
                <input type="text" id="buttontitle" name="buttontitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.buttontitle" required="required" >

                <div class="line line-dashed b-b line-lg"></div>

                Body Content
                <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>

              </div>


          </div>
        </div>

        <div class="col-sm-4">

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
            Page Banner
            </div>
            <div class="panel-body">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.banner }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="banner" name="banner" class="form-control" ng-model="page.banner"  placeholder="Paste Banner link here..." required="required" onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.banner, 'banner')">
            </div>
          </div>
          
          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
            Special Page
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-2">
                  <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                    <input type="checkbox" checked="checked" ng-model="page.specialpage">
                    <i></i>
                  </label>
                </div>
                <div class="col-sm-10">
                  <div ng-if="page.specialpage">
                    <select class="form-control" required="required" ng-model="page.action" ng-change="sample(page.action)">
                      <option value="{[{ data.action }]}" ng-repeat="data in actions"> {[{ data.name }]} </option>
                    </select>
                  </div>
                </div>
              </div>
          
            </div>
          </div> -->

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Meta
            </div>
            <div class="panel-body">
               Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" required="required" placeholder="Meta Title">

              <div class="line line-dashed b-b line-lg"></div>
                Description
              <input input="text"  id="metadesc" name="metadesc"  class="form-control" ng-model="page.metadesc" required="required" placeholder="Meta Description">

              <div class="line line-dashed b-b line-lg"></div>
                Keyword
              <input type="text" id="metatags" name="metatags" class="form-control" ng-model="page.metatags" required="required" placeholder="Meta Keyword">
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Thumbnail
            </div>
            <div class="panel-body">
              <img ng-show="page.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.imagethumb }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="imagethumb" name="imagethumb" class="form-control" ng-model="page.imagethumb"  placeholder="Paste Image Thumbnail link here..." required="required" onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.imagethumb, 'thumb')">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.imagethumbsubtitle"  placeholder="Paste Image Thumbnail Subtitle" required="required" >
              <div class="line line-dashed b-b line-lg"></div>
              <textarea type="text" id="thumbdesc" name="thumbdesc" class="form-control" ng-model="page.thumbdesc"  placeholder="Description" required="required" > </textarea>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body">
                Service Price
                <input type="text" id="serviceprice" name="serviceprice" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.serviceprice" required="required" >
              </div>

          </div>

          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar Switch
            </div>

            <div class="panel-body">

              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="a" ng-model="page.layout" value="1" ng-click="radio('1')" required="required" checked="true">
                  <i></i>
                  Full Page Layout
                </label>
              </div>

              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="a" ng-model="page.layout" value="2" ng-click="radio('2')" required="required">
                  <i></i>
                  Page with Sidebar Layout
                </label>
              </div>
            </div>
          </div> -->
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <i class="fa fa-dollar"></i> Prices
              <a ng-click="addPrice=true;showAdd=true" ng-hide="addPrice"><span class="pull-right"><i class="fa fa-plus-square"></i></span></a>
              <button  type="button" class="btn btn-default btn-xs pull-right" ng-show="addPrice" ng-click="priceCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
              <button  type="button" class="btn btn-default btn-xs pull-right" ng-show="showUpdate" ng-click=" updatePrice(price)" ng-disabled="!price.desc || !price.amount || price.amount <= 0"><i class="glyphicon glyphicon-edit"></i> Update</button>
              <button class="btn btn-default btn-xs pull-right " type="button" ng-show="addPrice && !showUpdate" ng-click="createPrice(price)" ng-disabled="!price.desc || !price.amount || price.amount <= 0"><i class="glyphicon  glyphicon-plus-sign "></i> Add</button>
            </div>
            <ul class="list-group no-radius">
              <li class="list-group-item animated fadeInDown" ng-show="addPrice || showUpdate">
                <div class="row">
                  <div class="col-md-6"><input class="form-control " name="desc"  ng-model="price.desc" placeholder="Description" type="text"></div>
                  <div class="col-md-3 padding-left0"><input class="form-control " name="amount" ng-model="price.amount" placeholder="Price" type="text" only-digits></div>
                  <div class="col-md-3 padding-left0"><input class="form-control " name="amount" ng-model="price.priceper" placeholder="d/h" type="text" value-date-time ></div>
                </div>
              </li>
              <li ng-repeat="p in prices track by $index "class="list-group-item service-price" ng-hide="currentEditPrice == $index">
                  <span class="pull-right">{[{p.amount}]} ({[{p.priceper}]}) </span>
                  <span class="service-price">{[{p.desc}]}
                    <span class="price-control-action"><a href="" ng-click="price.desc=p.desc; price.amount=p.amount;price.priceper=p.priceper;showUpdate=true; editPrice($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <span class="price-control-action"><a href="" ng-click="delete($index)"><i class="fa fa-times-circle"></i></a></span>
                  </span>
              </li>
            </ul>
          </div>
        </div>

        <!-- <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body">
                Service Price
                <input type="text" id="serviceprice" name="serviceprice" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.serviceprice" required="required" >
              </div>

          </div>
        </div>  -->

      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>


      <div  class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Image Gallery
              </div>




                <div class="panel-body">
                  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
                  <div class="loader" ng-show="imageloader">
                    <div class="loadercontainer">

                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                      Uploading your images please wait...

                    </div>

                  </div>

                  <div ng-show="imagecontent">

                    <div class="col-sml-12">
                      <div class="dragdropcenter">
                        <div ngf-drop ngf-select ng-model="files" class="drop-box"
                        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*,application/pdf">Drop images here or click to upload</div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="col-sm-3" ng-repeat="data in imagelist">
                     <a href="" ng-click="deletepagesimg(data.filename)" class="closebutton">&times;</a>
                      <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
                      <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}');">
                      </div>
                    </div>

                  </div>


                </div>

          </div>
        </div>

      </div>

  </div>
</fieldset>
</form>
