{{ content() }}
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create User</h1>
  <a id="top"></a>
</div>
<div  ng-controller="AddUserCtrl">
  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(user, file)" name="userform" id="userform">
    <fieldset ng-disabled="isSaving">
      <div class="wrapper-md" >
        <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Account Information
            </div>
            <div class="panel-body">

              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Username</label>
                <div class="col-sm-8">
                  <span class="label bg-danger" ng-show="validusrname">Username already taken. <br/></span>

                  <input type="text"  id="" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-keypress="chkusername(user.username)" required="required">
                  <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-8">
                  <span class="label bg-danger" ng-show="validemail">Email address already taken. <br/></span>
                  <input type="email" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required" >
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Password</label>
                <div class="col-sm-8">
                  <input type="password" id="password" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-change="chkpass(user.password, user.conpass)" ng-model="user.password" required="required" min-length="6" max-length="20">
                  <em class="text-muted">(allow 'a-zA-Z0-9', 6-20 length)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-8">
                   <span class="label bg-danger" ng-show="pwdconfirm">Password do not match! <br/></span>
                  <input type="password" id="conpass" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.conpass" ng-change="confirmpass(user.conpass)" required="required" >
                  <!-- <span ng-show='form.confirm_password.$error.validator'>Passwords do not match!</span> -->
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                 <label class="col-lg-3 control-label">User Role</label>
                  <div class="col-sm-8">
                   <div class="radio">
                     <label class="i-checks">
                       <input type="radio" name="userrole" value="Administrator" ng-model="user.userrole" required="required">
                       <i></i>
                       [ Administrator ] ~ Can access all the features in CMS including managing all content in the page, and creating user account and other function and features in CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="radio" name="userrole" value="Page Manager" ng-model="user.userrole" required="required">
                       <i></i>
                       [ Pages ] ~ had the access on managing the pages in the CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="radio" name="userrole" value="News/Blog Editor" ng-model="user.userrole" required="required">
                       <i></i>
                       [ News ] ~ <!-- Can access only the center page where he/she is working. -->had the access on managing the News/Blog posts in the CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="radio" name="userrole" value="Testimonial Editor" ng-model="user.userrole" required="required">
                       <i></i>
                       [ Testimonials ] ~ had the access on managing the Testimonials in the CMS.<!--  The editor can manage the news/blog content in all the center and in all the front end news/blog pages. -->
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="radio" name="userrole" value="Booking Manager" ng-model="user.userrole" required="required">
                       <i></i>
                       [ Booking ] ~ Can accessing only the booking page in the CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                 </div>
               </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              User Profile
            </div>

            <div class="panel-body">
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Profile Picture</label>
                <div class="col-sm-8">
                  <img ngf-src="file[0]" id="profpic" ngf-default-src="{[{base_url}]}/img/testidefaultimage.png" ngf-accept="'image/*'">
                  <label class="label_profile_pic btn btn-primary" id="change-picture" ngf-change="prepare(file)" ngf-select ng-model="file" ngf-multiple="false" required="required">Change Picture</label>
                </div>
              </div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-8">
                  <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" required="required" >
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-8">
                  <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname" required="required" >
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Birthdate</label>
                <div class="col-sm-8">
                  <div class="input-group w-md">
                    <span class="input-group-btn">
                      <input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" placeholder="yyyy-mm-dd">
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-8">
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                      <i></i>
                      Male
                    </label>
                  </div>
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                      <i></i>
                      Female
                    </label>
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="form-group hiddenoverflow">
                <label class="col-lg-2 control-label">Status</label>
                <div class="col-sm-8">
                  <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                    <input type="checkbox" checked="checked" ng-model="user.status" >
                    <i></i>
                  </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
              </div>
              </div>
            </div>
          </div>
          <div class="progress" style="width:97%; margin:0 20px;">
            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {[{ process }]}%;">
              <span class="sr-only">{[{ process }]}% Complete</span>
            </div>
          </div>
          <div class="panel-body" style="overflow: hidden; width : 100%" >
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default" href="/dashboard"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="userform.$invalid||validusrname==true||validemail==true||pwdconfirm==true||pwd==true">Submit</button>
            </footer>
          </div>

        </fieldset>
      </form>

 </div>
