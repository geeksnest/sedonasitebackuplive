{{ content() }}
<script type="text/ng-template" id="categoryAdd.html">
   <div ng-include="'/be/tpl/categoryAdd1.html'"></div>
</script>

<script type="text/ng-template" id="categoryDelete.html">
   <div ng-include="'/be/tpl/categoryDelete.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">News Category
        <button type="button" class=" pull-right btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus" style="width=100%;"></i>Add New Category
        </button>
    </h1>
    <a id="top"></a>

</div>

<fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

        <div class="row">

            <div class="col-sm-12">

                <div class="panel panel-default">
                    <div class="panel-heading font-bold">
                     Category List
                    </div>

                        <div class="panel-body">



                            <div class="row wrapper">
                                <div class="col-sm-5 m-b-xs" ng-show="keyword">
                                    <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                                </div>
                                <div class="col-sm-5 m-b-xs pull-right">
                                    <div class="input-group">
                                        <input class="input-sm form-control" name="searchtext" placeholder="Search" type="text" ng-model="searchtext">
                                        <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                    <thead>
                                        <tr>
                                            <th style="width:80%">Category Name</th>
                                            <th style="width:25%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody ng-show="loading">
                                    <tr colspan="2">
                                        <td>Loading Category List...</td>
                                    </tr>
                                    </tbody ng-hide="loading">
                                    <tbody>
                                        <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                                        <tr ng-repeat="mem in data.data" >
                                            <td>
                                            <span editable-text="mem.categoryname" onbeforesave="updatecategory($data, mem.categoryid)" e-pattern="[a-zA-Z0-9\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ mem.categoryname }]}</span>
                                            </td>
                                            <td>
                                            <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
                                            <a href="" ng-click="categoryDelete(mem.categoryid)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
                                            </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                </div>
            </div>

        </div>

        <div class="row" ng-hide="bigTotalItems==0 || loading">
            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                    <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                    <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                </footer>
            </div>
        </div>

    </div>
</fieldset>
