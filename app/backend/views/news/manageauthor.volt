{{ content() }}

<script type="text/ng-template" id="authorDelete.html">
  <div ng-include="'/be/tpl/authorDelete.html'"></div>
</script>

<script type="text/ng-template" id="authorEdit.html">
  <div ng-include="'/be/tpl/authorEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Authors</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formAuthor">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Authors List
            </div>


              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-5 m-b-xs pull-right">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>                               
                                <th style="width:25%">Name</th>
                                <th style="width:25%">Location</th>
                                <th style="width:20%">Occupation</th>
                                <th style="width:20%">Author Since</th>
                                <th style="width:10%">Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                        <tr >
                            <td colspan="4">Loading News</td>
                        </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr  ng-show="bigTotalItems==0"> <td colspan="4"> No records found! </td></tr>
                            <tr ng-repeat="mem in data.data">
                               
                                <td>{[{ mem.name }]}</td>
                                <td>{[{ mem.location }]}</td>
                                <td>{[{ mem.occupation }]}</td>
                                <td>{[{ mem.date_created }]}</td>
                                
                                <td>
                                    <a href="" ng-click="editauthor(mem.authorid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="deleteauthor(mem.authorid)"> <span class="label bg-danger">Delete</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
          <div class="panel-body">
              <footer class="panel-footer text-center bg-light lter">
                  <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                  <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
              </footer>
          </div>
      </div>

  </div>
</fieldset>
</form>