{{ content() }}

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit News</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveNews(news)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              News Information
            </div>

              <input type="hidden"  ng-model="news.newsid">
              <input type="hidden"  ng-model="news.datecreated">
              <div class="panel-body">

                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.title" required="required" ng-keyup="onnewstitle(news.title)" ng-change="onnewstitle(news.title)">
                <br>
                <b>News Slugs: </b>
                <input type="hidden" ng-model="news.slugs"><span ng-bind="news.slugs"></span>

                <div class="line line-dashed b-b line-lg"></div>

                Author
                <input type="text" class="form-control" ng-model="news.author" name="news.author" required="required" >

                <div class="line line-dashed b-b line-lg"></div>

                Date
                <div class="input-group w-md">
                  <span class="input-group-btn">
                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="news.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                  </span>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                News Location
                <div ui-module="select2">
                  <select ui-select2 ng-model="news.newslocation" class="form-control w-md" required="required">
                    <option value="Main Site">Main Site</option>
                    <option value="Glendale-AZ">Glendale-AZ</option>
                    <option value="Scottsdale">Scottsdale</option>
                    <option value="Sun City">Sun City</option>
                    <option value="Tempe">Tempe</option>
                    <option value="Anaheim Hills">Anaheim Hills</option>
                    <option value="Brea">Brea</option>
                    <option value="Burbank">Burbank</option>
                    <option value="Cerritos">Cerritos</option>
                    <option value="Chatsworth">Chatsworth</option>
                    <option value="Fremont">Fremont</option>
                    <option value="Garden Grove">Garden Grove</option>
                    <option value="Glendale">Glendale</option>
                    <option value="Irvine">Irvine</option>
                    <option value="Manteca">Manteca</option>
                    <option value="Monrovia">Monrovia</option>
                    <option value="Oceanside">Oceanside</option>
                    <option value="Pasadena">Pasadena</option>
                    <option value="Rolling Hills">Rolling Hills</option>
                    <option value="San Mateo">San Mateo</option>
                    <option value="San Ramon">San Ramon</option>
                    <option value="Santa Clara">Santa Clara</option>
                    <option value="Tarzana">Tarzana</option>
                    <option value="Torrance">Torrance</option>
                    <option value="Valley">Valley</option>
                  </select>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div ng-show='news.newslocation =="Main Site"'>
                Category
                <select class="form-control m-b" ng-model="news.category" ng-required='news.newslocation =="Main Site"' ng-options="cat.name as cat.name for cat in category">
                </select>
                

                <div class="line line-dashed b-b line-lg"></div>
                </div>

                Body Content
                <textarea class="ck-editor" ng-model="news.body" required="required"></textarea>

                
              </div>


          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News Banner
            </div>
            <div class="panel-body">
              <img ng-show="news.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{ news.banner }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="banner" name="banner" class="form-control" ng-model="news.banner"  placeholder="Paste Banner link here..." required="required" onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(news.banner)">
            </div>
          </div>
        </div>



      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>


      <div  class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Image Gallery
              </div>




                <div class="panel-body">
                  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
                  <div class="loader" ng-show="imageloader">
                    <div class="loadercontainer">
                      
                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                      Uploading your images please wait...

                    </div>
                    
                  </div>

                  <div ng-show="imagecontent">

                    <div class="col-sml-12">
                      <div class="dragdropcenter">
                        <div ngf-drop ngf-select ng-model="files" class="drop-box" 
                        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*,application/pdf">Drop images here or click to upload</div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="col-sm-3" ng-repeat="data in imagelist">
                      <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
                      <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{data.filename}]}');">
                      </div>
                    </div>

                  </div>


                </div>

          </div>
        </div>

      </div>

  </div>
</fieldset>
</form>