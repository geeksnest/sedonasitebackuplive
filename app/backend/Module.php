<?php

namespace Modules\Backend;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory;
use Modules\Backend\Plugins\Security as Sec;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'Modules\Backend\Controllers' => __DIR__ . '/controllers/',
			'Modules\Backend\Plugins' => __DIR__ . '/plugins/',
			'Modules\Backend\Models' => __DIR__ . '/models/',
			'Modules\Backend\Forms' => __DIR__ . '/forms/'
		));
		$loader->register();
	}

	public function registerServices($di)
	{

		/**
		 * Read configuration
		 */
		$config = include __DIR__ . "/config/config.php";

		$di['dispatcher'] = function() use ($di){
			$dispatcher = new \Phalcon\Mvc\Dispatcher();
			$dispatcher->setDefaultNamespace("Modules\Backend\Controllers");

		    //Create an event manager
		    $eventsManager = $di->getShared('eventsManager');

		    $security = new Sec($di);

		    /**
		     * We listen for events in the dispatcher using the Security plugin
		    */
		    $eventsManager->attach('dispatch', $security);

		    //Bind the eventsManager to the view component
		    $dispatcher->setEventsManager($eventsManager);

			return $dispatcher;
		};

		/**
		 * Setting up the view component
		 */
		$di['view'] = function() {

			$view = new \Phalcon\Mvc\View();

			$view->setViewsDir(__DIR__ . '/views/');
			$view->setLayoutsDir('layouts/');
			$view->setTemplateAfter('main');


			    $view->registerEngines(array(
			        '.volt' => function ($view, $di){

			            $volt = new VoltEngine($view, $di);

			            $volt->setOptions(array(
			                'compiledPath' => __DIR__ . '/cache/',
			                'compiledSeparator' => '_'
			            ));

			            return $volt;
			        },
			        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
			    ));		    
			return $view;
		};

		/**
		 * Database connection is created based in the parameters defined in the configuration file
		 */
		$di['db'] = function() use ($config) {
			return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
				"host" => $config->database->host,
				"username" => $config->database->username,
				"password" => $config->database->password,
				"dbname" => $config->database->name
			));
		};
		/**
		 * If the configuration specify the use of metadata adapter use it or use memory otherwise
		 */
		$di['modelsMetadata'] = function () {
		    return new \Phalcon\Mvc\Model\Metadata\Memory();
		};

		/**
		* Security set
		*/
		$di['security'] = function(){

		    $security = new \Phalcon\Security();

		    //Set the password hashing factor to 12 rounds
		    $security->setWorkFactor(12);

		    return $security;
		};

		/**
		* Flash Service
		*/
		$di['flash'] =  function() {  
		    $flash = new \Phalcon\Flash\Direct(array(
		        'warning' => 'alert alert-warning',
		        'error' => 'label label-danger',
		        'success' => 'alert alert-success',
		        'notice' => 'alert alert-info',
		    ));
		    return $flash;
		};
		/**
	    * ModelsManager
	    */
		$di['modelsManager'] =  function() {
		      return new \Phalcon\Mvc\Model\Manager();
		};
	}

}