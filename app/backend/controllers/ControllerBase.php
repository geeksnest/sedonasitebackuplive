<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
        $this->tag->setTitle("Sedona Healing Arts");
        $this->view->username = $this->session->get('auth');

        $this->createJsConfig();

        $auth = $this->session->get('auth');

        $curl = curl_init($this->config->application->ApiURL."/user/info/".$auth['pi_id']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        $decoded = json_decode($curl_response);
        curl_close($curl);

        $this->view->userinfo = $decoded;
    }

    private function createJsConfig(){
        $script="app.constant('Config', {
// This is a generated Config
// Any changes you make in this file will be replaced
// Place you changes in app/config.php file \n";
            foreach( $this->config->application as $key=>$val){
                $script .= $key . ' : "' . $val .'",' . "\n";
            }
        $script .= "});";
        $fileName="../public/be/js/scripts/config.js";

        $exist =  file_get_contents($fileName);
        if($exist == $script){

        }else{
            @file_put_contents($fileName, $script);
        }

        $this->view->base_url = $this->config->application->BaseURL;
    }
}
