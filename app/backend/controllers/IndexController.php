<?php

namespace Modules\Backend\Controllers;  

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as PI;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }
    public function logoutAction(){
        session_destroy();
        $this->flash->warning('You have successfully logout.');
        $this->view->pick("index/index");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function indexAction()
    {   
        $auth = $this->session->get('auth');
        if ($auth){
            $this->response->redirect('sedonaadmin/admin');
        }   
        ////////////////////////////////////////////////////////////////////
        $this->view->error = null;
        if ($this->request->isPost()) {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $shapass='';
            //$hashpass = $this->security->hash($password);



            $service_url = $this->config->application->ApiURL .'/user/login/'.$username.'/'.$password;

            $curl = curl_init($service_url);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $curl_response = curl_exec($curl);

            if ($curl_response === false) {
                $info = curl_getinfo($curl);
                curl_close($curl);
                die('error occured during curl exec. Additional info: ' . var_export($info));
            }
            curl_close($curl);
            $decoded = json_decode($curl_response);
            if(@$decoded->error){
                echo $decoded->error;
            }else{
                $this->_registerSession($decoded->success);
                $this->response->redirect('sedonaadmin/admin');
               /* $user = PI::findFirst("username='$username'");
                if($user){
                    $shapass = sha1($password);
                    if($shapass == $user->password){
                        $this->_registerSession($user);
                        $this->response->redirect('sedonaadmin/admin');
                    }

                }*/
            }
        }
        // $this->view->error = null;
        // if ($this->request->isPost()) {
        //     $username = $this->request->getPost('username');
        //     $password = $this->request->getPost('password');
        //     $shapass='';
        //     //$hashpass = $this->security->hash($password);
        //     $user = PI::findFirst("username='$username'");
        //     if($user){
        //         $shapass = sha1($password);
        //         if($shapass == $user->password){
        //             $this->_registerSession($user);
        //             $this->response->redirect('superagent/admin');
        //         }
        //     }
        //     $this->flash->warning('Wrong Username/Password' . $shapass);
        // }
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }

    private function _registerSession($user)
    {
        $this->session->set('auth', array(
            'pi_id' => $user->id,
            'pi_fullname' => $user->firstname .' '.$user->lastname,
            'pi_username' => $user->username
            ));

    //Set SuperAdmin
        if($user->userLevel){
            $this->session->set('SuperAgent', true );
        }
    }
}

