<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;

class ForgotpasswordController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function changepasswordAction($email, $token)
    {	
    	$this->view->email = $email;
    	$this->view->token = $token;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

