<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class SettingsController extends ControllerBase
{
    public function indexAction()
    {
    	 $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

